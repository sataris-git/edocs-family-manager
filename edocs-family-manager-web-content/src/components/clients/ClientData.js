import React from 'react';
import { Button, Form, FormGroup, Label, Input, Row, Col, InputGroup, InputGroupAddon, Alert } from 'reactstrap';
import axios from 'axios';
import { Error } from '../Error';


export class ClientData extends React.Component {
     constructor(props) {
          super(props)

          this.state = { 
               id: "",
               companyname: "... pobieranie danych",
               joinDate: "",
               patron: "... pobieranie danych",
               description: "... pobieranie danych",
               blockAll: true,

               editClientSuccessInfo: false,

               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }


     componentWillMount() {
          this.downloadClientData(this.props);
     }


     componentWillReceiveProps(nextProps) {
          if(this.props.clientId !== nextProps.clientId) {
               this.downloadClientData(nextProps);
          }

     }


     downloadClientData(props) {
          this.setState({ 
               id: "",
               companyname: "... pobieranie danych",
               joinDate: "",
               patron: "... pobieranie danych",
               description: "... pobieranie danych",
               blockAll: true
          })

          axios.get(`/edocs-family-manager/db/clients/${props.clientId}`)
          .then((resultGetClientData) => {
                    this.setState({ 
                         id:  resultGetClientData.data.id,
                         companyname: resultGetClientData.data.companyname,
                         joinDate: resultGetClientData.data.joinDate,
                         patron: resultGetClientData.data.patron,
                         description: resultGetClientData.data.description,
                         blockAll: false
                    })
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie informacji o kliencie",
                              message: `Nie udało się pobrać informacji o kliencie`,
                              data: error.response.data
                    }});
          });
     }


     handleChange = (e) => {
               this.setState({[e.target.name]: e.target.value
          })
     }


     //After submit form
     handleSubmit = (e) => {
          e.preventDefault();

          axios.post('/edocs-family-manager/db/clients', {
               id: e.target.id.value,
               companyname: e.target.companyname.value,
               joinDate: e.target.joinDate.value,
               patron: e.target.patron.value,
               description: e.target.description.value
             })
             .then((response) => {
                    //Show success alert
                    this.setState({editClientSuccessInfo: true})
                    this.props.downloadClientsList();
                    //After 2000ms - reload client details table
                    setTimeout( () => {
                         this.setState({editClientSuccessInfo: false})
                    }, 2000);
             })
             .catch((error) => {
               this.setState({                    
                    error: {
                         display: true,
                         title: "Edycja informacji o kliencie",
                         message: `Nie udało się edytować informacji o kliencie`,
                         data: error.response.data
               }});
             });
     }


      
     render() {
          return (
               <section id="ClientData">
               <h4>Dane klienta <br /> {this.state.companyname} </h4>
               <Form onSubmit={this.handleSubmit}>
<Row>
     <Col>
          <Input type="hidden" name="id" id="id" value={this.state.id} onChange={this.handleChange} disabled={this.state.blockAll} required/>
       
          <FormGroup>
               <Label for="clientname">*Nazwa klienta</Label>
               <InputGroup>
                    <InputGroupAddon addonType="prepend">{this.props.clientId}</InputGroupAddon>
                    <Input type="text" name="companyname" id="companyname" value={this.state.companyname} onChange={this.handleChange} disabled={this.state.blockAll} required/>
               </InputGroup>

          </FormGroup>

          <FormGroup>
               <Label for="joinDate">*Data dołączenia</Label>
               <Input type="date" name="joinDate" id="joinDate" value={this.state.joinDate} onChange={this.handleChange} disabled={this.state.blockAll} required />
          </FormGroup>

          <FormGroup>
               <Label for="patron">*Patron</Label>
               <Input type="text" name="patron" id="patron" value={this.state.patron} onChange={this.handleChange} disabled={this.state.blockAll} required/>
          </FormGroup>

          <Button onClick={this.props.onDeleteClick} value={this.state.id} color="danger" disabled={this.state.blockAll}>Usuń</Button>   
     </Col>

     <Col>
          <FormGroup className="description">
               <Label for="description">*Opis</Label>
               <Input type="textarea" name="description" id="description" value={this.state.description} onChange={this.handleChange} disabled={this.state.blockAll} required/>
          </FormGroup>
     </Col>
</Row>

<Row>
     <Col>          
          <Button color="warning" type="submit">Zapisz zmiany</Button>  
          <Alert  isOpen={this.state.editClientSuccessInfo} color="success">
               Zmiany zostały zapisane
          </Alert>
     </Col>
</Row>



</Form>

<Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

               </section>
          )
     }
}