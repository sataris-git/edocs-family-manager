import React from 'react';
//import ReactDOM from 'react-dom';
import axios from 'axios';
import moment from 'moment';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Input, Button, Table, Row, Col, Collapse } from 'reactstrap';

// import { AppsAndClients } from './Test.js';

import './AppsAndClients.css';

export class AppsAndClients extends React.Component {
  constructor(props) {
    super(props);
    this.getfocusedDetailsData = this.getfocusedDetailsData.bind(this);
    this.state = {
      focusedDetailsColor: false,
      idAppFromClientAppRow: '',
      appVersionFromClientAppRow: '',
      collapseAppDetalis: false,
    };
  }

  getfocusedDetailsData(idApp, idVersion) {
    this.setState({
      idAppFromClientAppRow: idApp,
      appVersionFromClientAppRow: idVersion,
      focusedDetailsColor: true,
      collapseAppDetalis: true
    });
  }

  render() {

    return (
      <div>
        <Row className="AppsAndClients">
          <Col>
            <AllApps key={this.getfocusedDetailsData}
              getfocusedDetailsData={this.getfocusedDetailsData}
              focusedDetailsColor={this.state.focusedDetailsColor}
              idAppFromClientAppRow={this.state.idAppFromClientAppRow}
              appVersionFromClientAppRow={this.state.appVersionFromClientAppRow}
              collapseAppDetalis={this.state.collapseAppDetalis}
            />
          </Col>
          <Col>
            <AllClients key={this.getfocusedDetailsData}
              getfocusedDetailsData={this.getfocusedDetailsData}
              focusedDetailsColor={this.state.focusedDetailsColor}
            />
          </Col>
        </Row>
      </div >
    );
  }
}


class AllApps extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      responseApps: [],
      appVersions: '',
    };
  }

  componentDidMount() {
    axios.get('/edocs-family-manager/db/apps')
      .then((responseApps) => {

        this.setState({
          responseApps: responseApps.data
        });
        //console.log(this.state.responseApps)
      })

      .catch(function (error) {
      });
  }

  render() {

    return (

      <div>
        <div className="card background_title">
          <div className="card-body">
            <svg height="100%" width="100%" viewBox="0 0 668 220">
              <defs>
                <linearGradient id="Gradient1">
                  <stop className="stop1" offset="0%" />
                  <stop className="stop2" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient2" x1="0%" y1="0%" x2="0%" y2="100%">
                  <stop className="stop3" offset="0%" />
                  <stop className="stop4" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient3" x1="0%">
                  <stop className="stop5" offset="0%" />
                  <stop className="stop6" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient4" x1="0%">
                  <stop className="stop7" offset="0%" />
                  <stop className="stop8" offset="100%" />
                </linearGradient>
              </defs>
              <g>
                <rect className="rect3" x="20" y="20" rx="20" ry="20"
                  width="628" height="180" />
                <rect className="rect2" x="10" y="10" rx="20" ry="20"
                  width="648" height="180" />
                <rect className="rect1" x="0" y="0" rx="20" ry="20"
                  width="668" height="180" />

                <path className="path1Style" d="M 0.018 132.579 L 0.032 161.392 C 0.021 164.311 4.798 170.961 6.675 173.064 C 9.784 175.91 14.589 178.804 18.784 179.952 L 650.454 180.047 C 651.067 179.922 651.606 180.142 652.161 179.998 C 659.392 178.129 661.755 174.922 662.24 174.153 C 664.304 171.675 667.347 165.994 667.964 161.821 L 668.028 34.213 C 668.292 34.321 668.213 33.932 668.022 34.223 C 665.097 38.69 611.259 63.18 581.097 65.611 C 519.973 70.537 507.277 58.706 467.74 56.283 C 426.806 57.139 407.182 69.378 366.42 106.734 C 351.939 120.005 334.785 119.916 327.29 120.001 C 307.864 120.222 297.147 117.372 276.031 111.564 C 248.753 104.062 209.635 86.189 162.321 88.119 C 115.007 90.049 83.264 122.259 54.072 139.185 C 38.94 149.577 -2.368 141.401 0.018 132.579 Z"></path>
                <path className="path2Style" d="M -0.006 106.678 L 0.005 162.292 C 0.005 162.292 1.314 169.077 5.261 173.15 C 9.547 177.572 17.257 179.982 17.257 179.982 L 651.447 179.993 C 651.447 179.993 658.12 178.525 662.313 174.313 C 666.835 169.771 667.996 161.945 667.996 161.945 L 667.988 88.119 C 672.298 89.883 622.201 83.514 590.647 90.106 C 533.113 102.125 524.559 86.417 470.09 83.478 C 445.445 82.148 445.594 87.662 393.069 102.431 C 374.062 107.776 375.224 106.657 352.139 106.699 C 334.864 106.731 319.571 100.02 297.73 94.212 C 270.389 86.942 239.39 71.416 195.718 78.868 C 170.741 83.13 139.77 107.619 113.734 125.862 C 85.267 145.808 82.564 147.527 72.621 147.931 C 50.739 152.023 -2.392 115.5 -0.006 106.678 Z"></path>
                <rect className="rect4" x="0" y="0" rx="20" ry="20"
                  width="668" height="180" />
                <g className="dotsApps">
                  <path className="dotsApps-light dotsApps--1" d="M 426 427 m -9.394 0 a 9.394 9.394 0 1 0 18.788 0 a 9.394 9.394 0 1 0 -18.788 0 Z M 426 427 m -5.637 0 a 5.637 5.637 0 0 1 11.274 0 a 5.637 5.637 0 0 1 -11.274 0 Z" data-bx-shape="ring 426 427 5.637 5.637 9.394 9.394 1@b9f4c1f4" transform="matrix(-0.707108, 0.707108, -0.707108, -0.707108, 660.309774, 133.003667)"></path>
                  <ellipse className="dotsApps-light dotsApps--2" transform="matrix(-0.83205, 0.554699, -0.554701, -0.832052, 577.608927, 0.558073)" cx="431.662" cy="145.409" rx="9.404" ry="9.404"></ellipse>
                  <ellipse className="dotsApps-light dotsApps--3" transform="matrix(-0.813725, 0.581232, -0.581236, -0.813731, 1069.317602, -191.993577)" cx="708.135" cy="135.628" rx="5.609" ry="5.609"></ellipse>
                  <ellipse className="dotsApps-light dotsApps--4" transform="matrix(-0.650795, 0.759264, -0.759259, -0.650789, 1210.345254, -516.98594)" cx="852.898" cy="97.158" rx="6.012" ry="6.012"></ellipse>
                  <path className="dotsApps-light dotsApps--5" d="M 1110 235 m -5.519 0 a 5.519 5.519 0 1 0 11.038 0 a 5.519 5.519 0 1 0 -11.038 0 Z M 1110 235 m -3.311 0 a 3.311 3.311 0 0 1 6.622 0 a 3.311 3.311 0 0 1 -6.622 0 Z" data-bx-shape="ring 1110 235 3.311 3.311 5.519 5.519 1@04fd7e16" transform="matrix(-0.853278, 0.521448, -0.521448, -0.853282, 1574.26718, -277.194362)"></path>
                </g>
                <text className="card-title" x="90" y="80" >Aplikacje rodziny EDOCS</text>
              </g>
            </svg>
          </div>
        </div>
        <div className="card card-header">
          <Row>
            <Col>
              Nazwa aplikacji
            </Col>
            <Col>
              Wersja aplikacji
            </Col>
            <Col xs="1">
            </Col>
          </Row>
        </div>
        {this.state.responseApps.map((app) =>
          <AppCategory key={app.id}
            appVersions={app.versions}
            appName={app.name}
            appCreateDate={app.createDate}
            appDescription={app.appsDescription}
            id={app.id}
            getfocusedDetailsData={this.props.getfocusedDetailsData}
            getfocusedDetailsAppVersion={this.props.getfocusedDetailsAppVersion}
            focusedDetailsColor={this.props.focusedDetailsColor}
            idAppFromClientAppRow={this.props.idAppFromClientAppRow}
            appVersionFromClientAppRow={this.props.appVersionFromClientAppRow}
            collapseAppDetalis={this.props.collapseAppDetalis}
          />
        )}
      </div>
    );
  }
}


class AppCategory extends React.Component {
  constructor(props) {
    super(props);
    this.getDependencies = this.getDependencies.bind(this);
    this.appVersionChange = this.appVersionChange.bind(this);
    this.toggleApp = this.toggleApp.bind(this);
    this.state = {
      responseDepend: [],
      value: this.props.appVersions.length ? this.props.appVersions[0].id : 0,     // condition ? true : false
      collapseApp: this.props.collapseAppDetalis,
    };
  }
  toggleApp() {
    this.setState({
      collapseApp: !this.state.collapseApp,
    });
  }

  componentWillMount() {
    //console.log('componentWillMount')
    this.getDependencies(this.state.value);
  }

  componentWillReceiveProps(nextProps, nextState) {
    //console.log('componentWillReceiveProps')
    if (this.props.id === nextProps.idAppFromClientAppRow) {
      //console.log('Id', this.props.id)
      //console.log('Version', this.state.value)
      //console.log('Id from Client', nextProps.idAppFromClientAppRow)
      //console.log('Version from Client', nextProps.appVersionFromClientAppRow)
      this.getDependencies(nextProps.appVersionFromClientAppRow);
      this.setState({
        value: nextProps.appVersionFromClientAppRow,
        collapseApp: nextProps.collapseAppDetalis,
      })
    } else {
      this.setState({
        collapseApp: !nextProps.collapseAppDetalis,

      })
    }
  }
  appVersionChange(event) {
    this.setState({ value: event.target.value });
  }

  getDependencies(idVersion) {
    axios.get('/edocs-family-manager/db/dependencies/' + idVersion)  //this.state.value)
      .then((responseDepend) => {

        this.setState({
          responseDepend: responseDepend.data
        });
      })

      .catch(function (error) {
      });
    //console.log('Version id is: ', idVersion);
  }


  render() {
    const collapseApp = this.state.collapseApp;
    let button = null;

    if (collapseApp) {
      button = <MinusButton onClick={this.toggleApp} />;
    } else {
      button = <PlusButton onClick={this.toggleApp} />;
    }

    const depRows = [];
    for (const [key, val] of Object.entries(this.state.responseDepend)) {
      depRows.push(
        <AppDependenceRow
          key={key}
          countDep={key}
          ValuesDep={val}
        />
      )
    }

    let background = null;
    if (this.props.focusedDetailsColor && (this.props.id === this.props.idAppFromClientAppRow)) {
      background = "app_card_level1_Change"
    }
    else {
      background = "app_card_level1"
    }

    return (
      <div className="card-block ">
        <div className="card" id={background} >
          <div className="card-body">
            <Row>
              <Col className="Textbold" onClick={this.toggleApp}>
                {this.props.appName}
              </Col>
              <Col>
                <Input className="dropdown" type="select" name="select" onClick={() => { this.getDependencies(this.state.value) }} onChange={this.appVersionChange} value={this.state.value}>
                  {this.props.appVersions.map((version) =>
                    <option className="dropdown_isOpen" value={version.id} key={version.id} >
                      {version.version} ({moment(version.creationDate).format('L')})
                    </option>
                  )}
                </Input>
              </Col>
              <Col xs="1" onClick={this.toggleApp}>
                <div className="collapsebutton">
                  {button}
                </div>
              </Col>
            </Row>
            <Collapse isOpen={this.state.collapseApp}>
              <div>
                <div className="helpTextColorApp">Data utworzenia aplikacji: <a className="info">{moment(this.props.appCreateDate).format('L')}</a></div>
                <div className="helpTextColorApp interspace">Opis aplikacji: <a className="info">{this.props.appDescription}</a></div>
                {depRows}
              </div>
            </Collapse>
          </div>
        </div>
      </div>

    );
  }
}

class MinusButton extends React.Component {
  render() {
    return (
      <div>
        <svg className="rotateButton" height="27px" width="27px" viewBox="0 0 300 300" >
        <defs>
                <linearGradient id="Gradient1">
                  <stop className="stop1" offset="0%" />
                  <stop className="stop2" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient5">
                  <stop className="stop2" offset="0%" />
                  <stop className="stop1" offset="100%" />
                </linearGradient>
              </defs>
          <g >
          <path  className="rect6" d="M 903 302 m -150 0 a 150 150 0 1 0 300 0 a 150 150 0 1 0 -300 0 Z M 903 302 m -125.576 0 a 125.576 125.576 0 0 1 251.152 0 a 125.576 125.576 0 0 1 -251.152 0 Z" data-bx-shape="ring 903 302 125.576 125.576 150 150 1@ec3bf25c"  transform="matrix(0.575181, -0.818026, 0.81803, 0.575175, -616.410196, 714.925526)"></path>
          <path  className="rect6" d="M 324.743 68.012 L 409.339 214.538 L 240.146 214.538 L 324.743 68.012 Z" data-bx-shape="triangle 240.146 68.012 169.193 146.526 0.5 0 1@7d664f41"  transform="matrix(-0.500002, 0.866024, -0.866025, -0.500001, 455.464939, -49.591223)"></path>
          </g>
        </svg>

      </div>
    );
  }
}
class MinusButtonCom extends React.Component {
  render() {
    return (
      <div>
     <svg className="rotateButton" height="22px" width="22px" viewBox="0 0 300 300" >
        <defs>
                <linearGradient id="Gradient1">
                  <stop className="stop1" offset="0%" />
                  <stop className="stop2" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient5">
                  <stop className="stop2" offset="0%" />
                  <stop className="stop1" offset="100%" />
                </linearGradient>
              </defs>
          <g >
          <path  className="rect6" d="M 903 302 m -150 0 a 150 150 0 1 0 300 0 a 150 150 0 1 0 -300 0 Z M 903 302 m -125.576 0 a 125.576 125.576 0 0 1 251.152 0 a 125.576 125.576 0 0 1 -251.152 0 Z" data-bx-shape="ring 903 302 125.576 125.576 150 150 1@ec3bf25c"  transform="matrix(0.575181, -0.818026, 0.81803, 0.575175, -616.410196, 714.925526)"></path>
          <path  className="rect6" d="M 324.743 68.012 L 409.339 214.538 L 240.146 214.538 L 324.743 68.012 Z" data-bx-shape="triangle 240.146 68.012 169.193 146.526 0.5 0 1@7d664f41"  transform="matrix(-0.500002, 0.866024, -0.866025, -0.500001, 455.464939, -49.591223)"></path>
          </g>
        </svg>
      </div>
    );
  }
}

class PlusButton extends React.Component {
  render() {
    return (
      <div>
        <svg className="rotateButton" height="27px" width="27px" viewBox="0 0 300 300" >
        <defs>
                <linearGradient id="Gradient1">
                  <stop className="stop1" offset="0%" />
                  <stop className="stop2" offset="100%" />
                </linearGradient>
                  <linearGradient id="Gradient5">
                  <stop className="stop2" offset="0%" />
                  <stop className="stop1" offset="100%" />
                </linearGradient>
              </defs>
          <g >
          <path className="rect1" d="M 903 302 m -150 0 a 150 150 0 1 0 300 0 a 150 150 0 1 0 -300 0 Z M 903 302 m -125.576 0 a 125.576 125.576 0 0 1 251.152 0 a 125.576 125.576 0 0 1 -251.152 0 Z" data-bx-shape="ring 903 302 125.576 125.576 150 150 1@ec3bf25c"  transform="matrix(0.575181, -0.818026, 0.81803, 0.575175, -616.410196, 714.925526)"></path>
          <path className="rect1" d="M 324.743 68.012 L 409.339 214.538 L 240.146 214.538 L 324.743 68.012 Z" data-bx-shape="triangle 240.146 68.012 169.193 146.526 0.5 0 1@7d664f41"  transform="matrix(0.50904, 0.860743, -0.860743, 0.50904, 125.535004, -215.018763)"></path>
          </g>
        </svg>
      </div>
    );
  }
}
class PlusButtonCom extends React.Component {
  render() {
    return (
      <div>
        <svg className="rotateButton" height="22px" width="22px" viewBox="0 0 300 300" >
        <defs>
                <linearGradient id="Gradient1">
                  <stop className="stop1" offset="0%" />
                  <stop className="stop2" offset="100%" />
                </linearGradient>
                  <linearGradient id="Gradient5">
                  <stop className="stop2" offset="0%" />
                  <stop className="stop1" offset="100%" />
                </linearGradient>
              </defs>
          <g >
          <path className="rect1" d="M 903 302 m -150 0 a 150 150 0 1 0 300 0 a 150 150 0 1 0 -300 0 Z M 903 302 m -125.576 0 a 125.576 125.576 0 0 1 251.152 0 a 125.576 125.576 0 0 1 -251.152 0 Z" data-bx-shape="ring 903 302 125.576 125.576 150 150 1@ec3bf25c"  transform="matrix(0.575181, -0.818026, 0.81803, 0.575175, -616.410196, 714.925526)"></path>
          <path className="rect1" d="M 324.743 68.012 L 409.339 214.538 L 240.146 214.538 L 324.743 68.012 Z" data-bx-shape="triangle 240.146 68.012 169.193 146.526 0.5 0 1@7d664f41"  transform="matrix(0.50904, 0.860743, -0.860743, 0.50904, 125.535004, -215.018763)"></path>
          </g>
        </svg>
      </div>
    );
  }
}

class AppDependenceRow extends React.Component {

  render() {
    return (
      <div>
        <div className="card card_depend_header ">
          <a id="depend">Zależności {this.props.countDep} rzędu</a>
        </div>
        <div className="card body_dependencies">
          <div className="card-body">
            {this.props.ValuesDep.map((dep) =>
              <Row className="interspace" key={dep.id}>
                <Col>
                  {dep.name}
                </Col>
                <Col>
                  {dep.version}
                </Col>
              </Row>
            )}
          </div>
        </div>
      </div>

    );
  }
}




class AllClients extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      responseClients: [],
      appVersions: ''
    };
  }

  componentDidMount() {

    axios.get('/edocs-family-manager/db/clients')
      .then((responseClients) => {

        this.setState({
          responseClients: responseClients.data
        });
      })

      .catch(function (error) {
      });
  }

  render() {
    return (

      <div>

        <div className="card background_title">
          <div className="card-body">
            <svg width="100%" height="100%" viewBox="0 0 668 220">
              <defs>
                <linearGradient id="Gradient1">
                  <stop className="stop1" offset="0%" />
                  <stop className="stop2" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient2" x1="0%" y1="0%" x2="0%" y2="100%">
                  <stop className="stop3" offset="0%" />
                  <stop className="stop4" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient3" x1="0%">
                  <stop className="stop5" offset="0%" />
                  <stop className="stop6" offset="100%" />
                </linearGradient>
                <linearGradient id="Gradient4" x1="0%">
                  <stop className="stop7" offset="0%" />
                  <stop className="stop8" offset="100%" />
                </linearGradient>
              </defs>
              <g>
                <rect className="rect3" x="20" y="20" rx="20" ry="20"
                  width="628" height="180" />
                <rect className="rect2" x="10" y="10" rx="20" ry="20"
                  width="648" height="180" />
                <rect className="rect1" x="0" y="0" rx="20" ry="20"
                  width="668" height="180" />

                <path className="path1Style" d="M 0.018 132.579 L 0.032 161.392 C 0.021 164.311 4.798 170.961 6.675 173.064 C 9.784 175.91 14.589 178.804 18.784 179.952 L 650.454 180.047 C 651.067 179.922 651.606 180.142 652.161 179.998 C 659.392 178.129 661.755 174.922 662.24 174.153 C 664.304 171.675 667.347 165.994 667.964 161.821 L 668.028 34.213 C 668.292 34.321 668.213 33.932 668.022 34.223 C 665.097 38.69 611.259 63.18 581.097 65.611 C 519.973 70.537 507.277 58.706 467.74 56.283 C 426.806 57.139 407.182 69.378 366.42 106.734 C 351.939 120.005 334.785 119.916 327.29 120.001 C 307.864 120.222 297.147 117.372 276.031 111.564 C 248.753 104.062 209.635 86.189 162.321 88.119 C 115.007 90.049 83.264 122.259 54.072 139.185 C 38.94 149.577 -2.368 141.401 0.018 132.579 Z"></path>
                <path className="path2Style" d="M -0.006 106.678 L 0.005 162.292 C 0.005 162.292 1.314 169.077 5.261 173.15 C 9.547 177.572 17.257 179.982 17.257 179.982 L 651.447 179.993 C 651.447 179.993 658.12 178.525 662.313 174.313 C 666.835 169.771 667.996 161.945 667.996 161.945 L 667.988 88.119 C 672.298 89.883 622.201 83.514 590.647 90.106 C 533.113 102.125 524.559 86.417 470.09 83.478 C 445.445 82.148 445.594 87.662 393.069 102.431 C 374.062 107.776 375.224 106.657 352.139 106.699 C 334.864 106.731 319.571 100.02 297.73 94.212 C 270.389 86.942 239.39 71.416 195.718 78.868 C 170.741 83.13 139.77 107.619 113.734 125.862 C 85.267 145.808 82.564 147.527 72.621 147.931 C 50.739 152.023 -2.392 115.5 -0.006 106.678 Z"></path>
                <rect className="rect4" x="0" y="0" rx="20" ry="20"
                  width="668" height="180" />
                <g className="dotsClients">
                  <path className="dotsClients-light dotsClients--1" d="M 1371 222 m -5.981 0 a 5.981 5.981 0 1 0 11.962 0 a 5.981 5.981 0 1 0 -11.962 0 Z M 1371 222 m -3.588 0 a 3.588 3.588 0 0 1 7.176 0 a 3.588 3.588 0 0 1 -7.176 0 Z" data-bx-shape="ring 1371 222 3.588 3.588 5.981 5.981 1@891ff440" transform="matrix(-0.707111, 0.707111, -0.707108, -0.707108, 1634.398564, -743.418248)"></path>
                  <path className="dotsClients-light dotsClients--2" d="M 691 348 m -8.759 0 a 8.759 8.759 0 1 0 17.518 0 a 8.759 8.759 0 1 0 -17.518 0 Z M 691 348 m -5.255 0 a 5.255 5.255 0 0 1 10.51 0 a 5.255 5.255 0 0 1 -10.51 0 Z" data-bx-shape="ring 691 348 5.255 5.255 8.759 8.759 1@821182fe" transform="matrix(-0.739938, 0.672671, -0.672675, -0.739941, 910.202319, -81.509019)"></path>
                  <ellipse className="dotsClients-light dotsClients--3" transform="matrix(-0.707102, 0.707102, -0.707107, -0.707105, 1089.661188, -294.311022)" cx="747.258" cy="152.581" rx="5.533" ry="5.533"></ellipse>
                  <ellipse className="dotsClients-light dotsClients--4" transform="matrix(-0.74741, 0.664362, -0.664363, -0.74741, 498.751362, -112.507223)" cx="419.271" cy="95.2" rx="7.473" ry="7.473"></ellipse>
                  <ellipse className="dotsClients-light dotsClients--5" transform="matrix(-0.780856, 0.624688, -0.624692, -0.780869, 1231.231346, -284.900051)" cx="807.9" cy="138.236" rx="4.175" ry="4.175"></ellipse>
                </g>
                <text x="160" y="80" >Klienci firmy Żbik</text>
              </g>
            </svg>
          </div>
        </div>

        <div className="card card-header">
          <Row>
            <Col>
              Nazwa klienta
                </Col>
            <Col>
              Patron klienta
                </Col>
            <Col xs="1">
            </Col>
          </Row>
        </div>
        {this.state.responseClients.map((client) =>
          <ClientCategory key={client.id}
            clientName={client.companyname}
            clientId={client.id}
            clientPatron={client.patron}
            clientJoinDate={client.joinDate}
            clientDescription={client.description}
            getfocusedDetailsData={this.props.getfocusedDetailsData}
            focusedDetailsColor={this.props.focusedDetailsColor}
          />
        )}
      </div>
    );
  }
}

class ClientCategory extends React.Component {
  constructor(props) {
    super(props);
    this.toggleClient = this.toggleClient.bind(this);
    this.state = {
      responseClientData: [],
      collapseClient: false,
    };
  }

  toggleClient() {
    this.setState({ collapseClient: !this.state.collapseClient });
  }

  componentDidMount() {
    axios.get('/edocs-family-manager/db/clients/' + this.props.clientId)
      .then((responseClientData) => {

        this.setState({
          responseClientData: responseClientData.data
        });
        //console.log(this.props.clientId, this.state.responseClientData);
      })
      .catch(function (error) {
      });
  }


  render() {
    //const{responseClientData, collapseClient} = this.state;
    const collapseClient = this.state.collapseClient;
    let button = null;
    if (collapseClient) {
      button = <MinusButton onClick={this.toggleClient} />;
    } else {
      button = <PlusButton onClick={this.toggleClient} />;
    }


    return (

      <div className="card-block">
        <div className="card client_card_level1">
          <div className="card-body">
            <Row>
              <Col className="Textbold" onClick={this.toggleClient}>
                {this.props.clientName}
              </Col>
              <Col onClick={this.toggleClient}>
                {this.props.clientPatron}
              </Col>
              <Col className="collapsebutton" xs="1" onClick={this.toggleClient}>
                {button}
              </Col>
            </Row>
            <Collapse isOpen={this.state.collapseClient}>
              <div>
                <div className="helpTextColorClient">Data dołączenia klienta: <a className="info">{moment(this.props.clientJoinDate).format('L')}</a></div>
                <div className="helpTextColorClient interspace">Opis klienta: <a className="info">{this.props.clientDescription}</a></div>
                <div className="card-block">
                  <div className="card client_card_level2">
                    <div className="card-body">
                      <h6>Aplikacje klienta</h6>

                      <div className="card card-header">
                        <Row>
                          <Col xs="3">
                            Nazwa
                              </Col>
                          <Col xs="3">
                            Wersja
                              </Col>
                          <Col xs="3">
                            Implementacja
                              </Col>
                          <Col xs="3">
                            Wyłączenie
                              </Col>
                        </Row>
                      </div>

                      {this.state.responseClientData.clientApps && this.state.responseClientData.clientApps.map((appRow) =>
                        <ClientAppRow key={appRow.id}
                          clientAppImplementDate={appRow.implementDate}
                          clientAppDisableDate={appRow.disableDate}
                          clientVersionId={appRow.appVersionId}
                          clientImplementDescription={appRow.implementDescription}
                          clientAppId={appRow.appVersionId.appId.id}
                          clientId={this.props.clientId}
                          focusedDetailsColor={this.props.focusedDetailsColor}
                          getfocusedDetailsData={this.props.getfocusedDetailsData}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </Collapse>
          </div>
        </div>
      </div>

    );
  }
}


class ClientAppRow extends React.Component {
  constructor(props) {
    super(props);
    this.toggleComment = this.toggleComment.bind(this);
    this.versionHistoryButton = this.versionHistoryButton.bind(this);
    this.focusedDetailsDataFromClientAppRow = this.focusedDetailsDataFromClientAppRow.bind(this);
    this.idApp = this.props.clientAppId;
    this.idVersion = this.props.clientVersionId.id;
    this.state = {
      responseAppHistory: [],
      versionHistory: false,
      collapseComment: false,
    };
  }

  focusedDetailsDataFromClientAppRow() {
    this.props.getfocusedDetailsData(this.idApp, this.idVersion)
  }

  versionHistoryButton() {
    this.setState({ versionHistory: !this.state.versionHistory });
  }

  toggleComment() {
    this.setState({ collapseComment: !this.state.collapseComment });
  }

  componentDidMount() {
    //console.log(this.props.clientAppId, this.props.clientId);
    axios.post('/edocs-family-manager/db/clients/apphistory', {
      "clientId": this.props.clientId,
      "appId": this.props.clientAppId
    })
      .then((responseAppHistory) => {

        this.setState({
          responseAppHistory: responseAppHistory.data
        });
        //console.log(this.props.clientAppId, responseAppHistory.data);
      })

      .catch(function (error) {
      });
  }

  render() {
    const collapseComment = this.state.collapseComment;
    let button = null;
    let comment = '';
    let commentEnd = '';
    let commentDots = '';
    let disableDate = null;

    if (this.props.clientImplementDescription.length > 52) {
      comment = this.props.clientImplementDescription.slice(0, 52)
      commentEnd = this.props.clientImplementDescription.slice(52)
      commentDots = '...'
      if (collapseComment) {
        button = <MinusButtonCom onClick1={this.toggleComment} />;
      } else {
        button = <PlusButtonCom onClick1={this.toggleComment} />;
      }
    }
    else {
      comment = this.props.clientImplementDescription
    }

    if (this.props.clientAppDisableDate === null) {
      disableDate = null;
    } else {
      disableDate = moment(this.props.clientAppDisableDate).format('L')
    }

    return (
      <div className="card-block">
        <div className="card client_card_level3">
          <div className="card-body">
            <Row id="row">
              <Col xs="3">
                {this.props.clientVersionId.appId.name}
              </Col>
              <Col xs="3">
                {this.props.clientVersionId.version}
              </Col>
              <Col xs="3">
                {moment(this.props.clientAppImplementDate).format('L')}
              </Col>
              <Col xs="3">
                {disableDate}
              </Col>
            </Row>
            <Row>
              <Col>
                <Button color="primary" className="client_button" onClick={this.versionHistoryButton}>Historia wersji</Button>
              </Col>
              <Col>
                <Button color="primary" className="client_button" onClick={this.focusedDetailsDataFromClientAppRow}>Szczegóły</Button>
              </Col>
            </Row>
            <div className="card-block">
              <div className="card comment_card">
                <div className="card-body" >
                  <Row onClick={this.toggleComment}>
                    <Col >
                      <a className="helpTextColorClient">Komentarz:</a> {comment}{commentDots}
                    </Col>

                    <Col className="collapsebutton" xs="1">
                      {button}
                    </Col>
                  </Row>
                  <Collapse isOpen={this.state.collapseComment}>
                    {commentEnd}
                  </Collapse>

                </div>
              </div>
            </div>
            <Collapse isOpen={this.state.versionHistory}>
              <div>
                <div className="card-block">
                  <div className="card client_table_version_history">
                    <div className="card-body">
                      <h5 className="title_version_history">Historia wersji</h5>
                      <Table responsive striped>
                        <thead>
                          <tr className="body_version_history">
                            <th>Wersja</th>
                            <th>Implementacja</th>
                            <th>Wyłączenie</th>
                            <th>Komentarz</th>
                          </tr>
                        </thead>
                        {this.state.responseAppHistory.map((history) =>
                          <ClientVersionHistory key={history.id}
                            historyVersion={history.version}
                            historyImplementDate={history.implementDate}
                            historyDisableDate={history.disableDate}
                            historyImplementDescription={history.implementDescription}
                          />
                        )}
                      </Table>
                    </div>
                  </div>
                </div>
              </div>
            </Collapse>
          </div>
        </div>
      </div>
    );
  }
}

class ClientVersionHistory extends React.Component {
  render() {
    //biblioteka moment.js użyta w celu konwersji daty z milisekund na format daty
    return (
      <tbody>
        <tr className="body_version_history">
          <td>{this.props.historyVersion}</td>
          <td>{moment(this.props.historyImplementDate).format('L')}</td>
          <td>{this.props.historyDisableDate}</td>
          <td>{this.props.historyImplementDescription}</td>
        </tr>
      </tbody>
    );
  }
}




