package com.edocs.family.mainpack.controllers;

import com.edocs.family.mainpack.DAO.DependencyReaderDAO;
import com.edocs.family.mainpack.services.AutomaticallyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author John
 */

@RestController
@RequestMapping("/db/automation")
public class AutomaticallyController {
    
    
    @Autowired
    public AutomaticallyService automaticallyService;
    
    
    
    //----- POST methods
    
    //Test
    @RequestMapping(method = RequestMethod.POST) 
    public void addAppsAndDependencies(@RequestBody DependencyReaderDAO csharpRequest) {
        
        automaticallyService.addAppsAndDependencies(csharpRequest);
         
    }
    
    
    
    
}
