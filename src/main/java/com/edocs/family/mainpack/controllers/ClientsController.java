/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.controllers;

import com.edocs.family.mainpack.DAO.ClientAppHistoryDAO;
import com.edocs.family.DTO.AppsAllTO;
import com.edocs.family.DTO.ClientsImplementHistoryTO;
import com.edocs.family.DTO.ClientsInfoTO;
import com.edocs.family.mainpack.entities.ClientsApps;
import com.edocs.family.mainpack.entities.ClientsInfo;
import com.edocs.family.mainpack.repository.ClientsAppsRepository;
import com.edocs.family.mainpack.repository.ClientsInfoRepository;
import com.edocs.family.mainpack.services.ClientsService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



/**
 *
 * @author PS
 */


@RestController
@RequestMapping("/db/clients")
public class ClientsController {
    
    @Autowired
    public ClientsService clientsService;
    
    @Autowired
    public ClientsInfoRepository clientsInfoRepository;
    
    @Autowired
    public ClientsAppsRepository clientsAppsRepository;
    
    
    //----- GET methods
    
    
    //Get all client list
    @RequestMapping(method = RequestMethod.GET)
    public List<ClientsInfo> allClientsList() {

        return clientsService.findAllClients();
    }
    
    
    
    
    
    //Get information about client
    @RequestMapping(value = "/{id}",
                    method = RequestMethod.GET,
                    produces = "application/json")
    public ClientsInfoTO listReturningDetails(@PathVariable("id") int id) {
        return clientsService.findByid(id);
    }
    
        
    //Get map of using apps by client
    @RequestMapping(value = "/{id}/usingapps",
                    method = RequestMethod.GET,
                    produces = "application/json")
    public Map<Integer, AppsAllTO> clientUsingApps(@PathVariable("id") int id) {
        return clientsService.clientUsingApps(id);
    }
    
    //----- POST methods

    
    //Add new client
    @RequestMapping(method = RequestMethod.POST)
    public void addClient(@RequestBody ClientsInfo clientsInfo) {
        clientsInfoRepository.save(clientsInfo);
    }
    
    
    
    //Show app history implementation of user
    @RequestMapping(value = "/apphistory",
                    method = RequestMethod.POST)
    public List<ClientsImplementHistoryTO> clientAppsHistory(@RequestBody ClientAppHistoryDAO clientAppHistoryDAO) {
        return clientsService.clientAppsHistory(clientAppHistoryDAO);
    }
    

    
    //Add new app to client or change existing one (clients_apps table)
    @RequestMapping(value = "/addclientapp",
                    method = RequestMethod.POST)
    public void setClientAppVersion(@RequestBody ClientsApps clientsApps) {
        clientsAppsRepository.save(clientsApps);
    }
    
    
            
    //Change existing app version (clients_apps table)
    @RequestMapping(value = "/changeversion/{clientAppsId}/{appVerId}",
                    method = RequestMethod.POST)
    public void setClientAppVersion(@PathVariable int clientAppsId, @PathVariable int appVerId) {
        clientsService.setClientAppVersion(clientAppsId, appVerId);
    }
    
    
    //----- DELETE methods
        
    //Delete client
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) 
    public void deleteClient(@PathVariable int id) {
        clientsInfoRepository.delete(id); 
    }
    

    //Delete app version implement
    @RequestMapping(value = "/deleteversion/{id}", method = RequestMethod.DELETE) 
    public void deleteClientAppVersion(@PathVariable int id) {
        clientsAppsRepository.delete(id); 
    }
    
    
}
