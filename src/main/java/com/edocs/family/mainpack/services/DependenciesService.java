/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.services;

import com.edocs.family.DTO.AppVersionsDependencies;
import com.edocs.family.DTO.AppVersionsMapDependenciesTO;
import com.edocs.family.mainpack.entities.AppsVersions;
import com.edocs.family.mainpack.entities.Dependencies;
import com.edocs.family.mainpack.repository.AppsVersionsRepository;
import com.edocs.family.mainpack.repository.DependenciesRepository;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author PS
 */

@Service
public class DependenciesService {
    
    @Autowired
    public DependenciesRepository dependenciesRepository;
    
    @Autowired
    public AppsVersionsRepository appsVersionsRepository;
    
    
    @Autowired
    public AppsVersionsService appsVersionsService;
    
    
    
    public List<Integer> parentOf(int id) {
        List<Integer> dtoList = new ArrayList<>();
        dependenciesRepository.parentOfRank(id).forEach((i) -> {
            Integer dto = i.getChildAppVersionId().getId();
            dtoList.add(dto);
        });
       
        return dtoList;
    }
    
    
    public List<Integer> childOf(int id) {
        List<Integer> dtoList = new ArrayList<>();
        dependenciesRepository.childOf(id).forEach((i) -> {
            Integer dto = i.getParentAppVersionId().getId();
            dtoList.add(dto);
        });
       
        return dtoList;
    }
    
    
    
    
    public List<AppVersionsDependencies> getChildrens(int id) {
        ModelMapper modelMapper = new ModelMapper();
        List<AppVersionsDependencies> dtoList = new ArrayList<>();
        
        dependenciesRepository.parentOfRank(id).forEach((i) -> {
              
            AppsVersions appVersion = appsVersionsRepository.findByid(i.getChildAppVersionId().getId());
            AppVersionsDependencies dto = modelMapper.map(appVersion, AppVersionsDependencies.class);
            
            dto.setDependencyId(i.getId());
            
            dtoList.add(dto);
        });
        
        return dtoList;
    }
    
    
    
    
    
    
    
    public List<AppVersionsDependencies> getParents(int id) {
        ModelMapper modelMapper = new ModelMapper();
        List<AppVersionsDependencies> dtoList = new ArrayList<>();
        
        dependenciesRepository.childOf(id).forEach((i) -> {
              
            AppsVersions appVersion = appsVersionsRepository.findByid(i.getParentAppVersionId().getId());
            AppVersionsDependencies dto = modelMapper.map(appVersion, AppVersionsDependencies.class);
            
            dto.setDependencyId(i.getId());
            
            dtoList.add(dto);
        });
        
        return dtoList;
    }
    
    
    
    
    
    
    
    public Map<Integer, List<AppVersionsMapDependenciesTO>> mapDependencies(int id) {
        List<AppVersionsDependencies> versions = this.getParents(id);
        
        ModelMapper modelMapper = new ModelMapper();
        List<AppVersionsMapDependenciesTO> rank_1 = new ArrayList<>();
        Map<Integer, List<AppVersionsMapDependenciesTO>> mapdto = new HashMap<>();
        
        versions.forEach((i) -> { 
        
            //Mapping object to DTO Class
            AppVersionsMapDependenciesTO dto = modelMapper.map(i, AppVersionsMapDependenciesTO.class);
            
            //Set Name in DTO - from DAO
            dto.setName(i.getAppId().getName());

            int appRank = i.getAppId().getAppRank();
            
            //Get map index value
            List<AppVersionsMapDependenciesTO> indexValue = mapdto.get(appRank);
            
            if(indexValue == null) {
                //If indexValue doesn't have even empty list (null)
                mapdto.put(appRank, new ArrayList<>());
                indexValue = mapdto.get(appRank);
            }

            //Set in map index value local variable - new position
            indexValue.add(dto);
            
            //Update map as new position
            mapdto.put(i.getAppId().getAppRank(), indexValue);
        });
        
        return mapdto;
    }
    
    
    
    
    
    
    
    public void deleteDependency(int id) {
        dependenciesRepository.delete(id); 
    }
    
    
    
    
    public void addDependency(Dependencies dependencies) {
        dependenciesRepository.save(dependencies);
    } 
    
    
}




    