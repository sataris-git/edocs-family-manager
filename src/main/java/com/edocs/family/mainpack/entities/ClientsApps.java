/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author John
 */
@Entity
@Table(name = "clients_apps")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClientsApps.findAll", query = "SELECT c FROM ClientsApps c")
    , @NamedQuery(name = "ClientsApps.findById", query = "SELECT c FROM ClientsApps c WHERE c.id = :id")
    , @NamedQuery(name = "ClientsApps.findByImplementDate", query = "SELECT c FROM ClientsApps c WHERE c.implementDate = :implementDate")
    , @NamedQuery(name = "ClientsApps.findByDisableDate", query = "SELECT c FROM ClientsApps c WHERE c.disableDate = :disableDate")
    , @NamedQuery(name = "ClientsApps.findByImplementDescription", query = "SELECT c FROM ClientsApps c WHERE c.implementDescription = :implementDescription")})
public class ClientsApps implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "implement_date")
    @Temporal(TemporalType.DATE)
    private Date implementDate;
    @Column(name = "disable_date")
    @Temporal(TemporalType.DATE)
    private Date disableDate;
    @Size(max = 2147483647)
    @Column(name = "implement_description")
    private String implementDescription;
    @JoinColumn(name = "app_version_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AppsVersions appVersionId;
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ClientsInfo clientId;

    public ClientsApps() {
    }

    public ClientsApps(Integer id) {
        this.id = id;
    }

    public ClientsApps(Integer id, Date implementDate) {
        this.id = id;
        this.implementDate = implementDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getImplementDate() {
        return implementDate;
    }

    public void setImplementDate(Date implementDate) {
        this.implementDate = implementDate;
    }

    public Date getDisableDate() {
        return disableDate;
    }

    public void setDisableDate(Date disableDate) {
        this.disableDate = disableDate;
    }

    public String getImplementDescription() {
        return implementDescription;
    }

    public void setImplementDescription(String implementDescription) {
        this.implementDescription = implementDescription;
    }

    public AppsVersions getAppVersionId() {
        return appVersionId;
    }

    public void setAppVersionId(AppsVersions appVersionId) {
        this.appVersionId = appVersionId;
    }

    public ClientsInfo getClientId() {
        return clientId;
    }

    public void setClientId(ClientsInfo clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientsApps)) {
            return false;
        }
        ClientsApps other = (ClientsApps) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.edocs.family.mainpack.entities.ClientsApps[ id=" + id + " ]";
    }
    
}
