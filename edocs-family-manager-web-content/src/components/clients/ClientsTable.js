import React from 'react';
import { Table, Button } from 'reactstrap';
import { Loading } from '../Loading'

export class ClientsTable extends React.Component {
     constructor(props) {
          super(props)

          this.state = { 
               all_clients: props.clientsList,
               disableButtons: props.disableButtons,
               clientEdit: props.clientEdit
          }
     }

     componentWillReceiveProps(nextProps) {
          //Set results when recived new props
          this.setState({
                    all_clients: nextProps.clientsList,
                    disableButtons: nextProps.disableButtons,
                    clientEdit: nextProps.clientEdit
               })
     }


     render() {
          let tableContent = <tr><td style={{textAlign: 'center'}} colSpan="4"><Loading /></td></tr>;

          //When LOADED.
          if(Array.isArray(this.state.all_clients) ) {
               tableContent = this.state.all_clients.map((element) => { 
               
                    let selectedRow;
                    if(parseInt(this.state.clientEdit, 10) === parseInt(element.id, 10) ) {
                         selectedRow = "selected"
                    }

                    return (
                         <tr key={element.id} className={selectedRow} >
                              <th scope="row">{element.id}</th>
                              <td>{element.companyname}</td>
                              <td><Button color="info"   onClick={this.props.onEditClick} value={element.id} disabled={this.state.disableButtons} >Edytuj</Button></td>
                              <td><Button color="danger" onClick={this.props.onDeleteClick} value={element.id} disabled={this.state.disableButtons} >Usuń</Button></td>
                         </tr>
                    )});
          }


          return (
               <Table striped id="ClientsTable">
                    <thead>
                         <tr>
                              <th>#</th>
                              <th>Nazwa</th>
                              <th>Akcja</th>
                              <th></th>
                         </tr>
                    </thead>
                    <tbody>
                          {tableContent}
                    </tbody>
               </Table>
          )
     }
}