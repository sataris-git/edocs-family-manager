import React from 'react';
import axios from 'axios';

import { Button, Form, FormGroup, Label, Input, Row, Col, Alert } from 'reactstrap';
import { Error } from '../Error';

export class ClientAppAdd extends React.Component {
     constructor(props) {
          super(props)

          this.state = {
               all_clients: props.clientsList,
               all_apps: props.appsList,
               app_versions: "* Wybierz aplikacje *",
               client_setted: false,
               clientSuccessAddAppInfo: false,
               disabledInputs: { 
                    clientaddappname: true,
                    clientaddapp: true,
                    clientaddappversion: true,
                    desc: true,
                    implementdate: true
               },
               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }

     
     componentDidMount() {
          this.componentWillReceiveProps(this.props);
          this.props.appearAnimate("#ClientAppAdd"); 
     }


     componentWillReceiveProps(nextProps) {
          //Set results when recived new props
          this.setState((prevState) => {
               let disabledInputs = prevState.disabledInputs;

               if(Array.isArray(nextProps.clientsList))
               disabledInputs.clientaddappname = false;
               
               if(Array.isArray(nextProps.appsList))
               disabledInputs.clientaddapp = false;

               return ({
                    all_clients: nextProps.clientsList,
                    all_apps: nextProps.appsList,
                    disabledInputs: disabledInputs
               })
          });


     }

     

     //After client choose
     handle_clientset = (e) => {
          this.setState({  client_setted: true });
     }



     //After App choose
     handle_clientaddappChange = (e) => {
          this.setState((prevState) => {
               let disabledInputs = prevState.disabledInputs;
               disabledInputs.clientaddappversion = true;

               return ({ 
                    app_versions: "... pobieranie danych",
                    disabledInputs: disabledInputs
               })
          });



          //Load app versions data
          axios.get(`/edocs-family-manager/db/apps/${e.target.value}`)
          .then((result) => {

                    this.setState((prevState) => {

                         let disabledInputs = prevState.disabledInputs;
                         disabledInputs.clientaddappversion = false;

                         return ({ 
                              app_versions: result.data.versions,
                              disabledInputs: disabledInputs
                         })
                    });
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie wersji aplikacji",
                              message: `Nie udało się pobrać wersji aplikacji`,
                              data: error.response.data
                    }});
          });
     }


     //After AppVersion choose
     handle_clientaddappversionChange = (e) => {
          this.setState((prevState) => {
               let disabledInputs = prevState.disabledInputs;

                    disabledInputs.desc = false;
                    disabledInputs.implementdate = false;

               return ({ 
                    disabledInputs: disabledInputs
               })
          });
     
     }

     //After submit form
     handleSubmit = (e) => {
          e.preventDefault();

          let form = e.target;

          //Disable "ADD" button, prevent double add 
          form.querySelector('button').setAttribute("disabled", true)

          axios.post('/edocs-family-manager/db/clients/addclientapp', {
               clientId: parseInt(e.target.clientaddappname.value, 10),
               implementDate: e.target.implementdate.value,
               disableDate: null,
               implementDescription: e.target.desc.value,
               appVersionId: parseInt(e.target.clientaddappversion.value, 10)
             })
             .then((responseClientAddApp) => {
                    //If all is ok
                    if(responseClientAddApp.status === 200) {
                         this.clientSuccessAddAppInfo();
                         //Reset inputs in form, and enable "ADD" button
                         form.reset();
                         form.querySelector('button').removeAttribute("disabled");
                    }
             })
             .catch((error) => {
               this.setState({                    
                    error: {
                         display: true,
                         title: "Dodawanie aplikacji klienta",
                         message: "Nie udało się dodać aplikacji klienta do bazy danych.",
                         data: error.response.data
               }});
             });
     }


     clientSuccessAddAppInfo() {
          this.setState({
               clientSuccessAddAppInfo: true,
               client_setted: false,
               disabledInputs: { 
                    clientaddappname: false,
                    clientaddapp: false,
                    clientaddappversion: true,
                    desc: true,
                    implementdate: true
               }
          })

          setTimeout( () => {
               this.setState({clientSuccessAddAppInfo: false})
          }, 2000);
     }


     render() {

          let allClientsArray = <option>{this.state.all_clients}</option>;
          if(Array.isArray(this.state.all_clients) ) {
               allClientsArray = this.state.all_clients.map((element) =>
                    <option key={element.id} value={element.id}>{element.companyname}</option>
               );
               allClientsArray.unshift(<option key='default' value="" disabled={this.state.client_setted}>-- Wybierz klienta --</option>)
          } 


          let allAppsArray = <option>{this.state.all_apps}</option>;
          if(Array.isArray(this.state.all_apps) ) {
               allAppsArray = this.state.all_apps.map((element) =>
                    <option key={element.id} value={element.id}>{element.name}</option>
               );
               allAppsArray.unshift(<option key='default' value="" disabled={!this.state.disabledInputs.clientaddappversion}>-- Wybierz aplikacje --</option>)
          }


          let allAppsVersionsArray = <option>{this.state.app_versions}</option>;
          if(Array.isArray(this.state.app_versions) ) {
               allAppsVersionsArray = this.state.app_versions.map((element) =>
                    <option key={element.id} value={element.id}>{element.version}</option>
               );
               allAppsVersionsArray.unshift(<option key='default' value="" disabled={!this.state.disabledInputs.implementdate}>-- Wybierz wersje aplikacji --</option>)
          }
          
          

          return (
<section id="ClientAppAdd">
     <h3>Dodawanie aplikacji klienta</h3>

     
     <Form onSubmit={this.handleSubmit}>
     <FormGroup>
               <Label for="clientaddappname">*Nazwa klienta</Label>
               <Input type="select" name="clientaddappname" id="clientaddappname" onChange={this.handle_clientset} disabled={this.state.disabledInputs.clientaddappname} required>
                    {allClientsArray}
               </Input>
          </FormGroup>


          <FormGroup>
               <Label for="clientaddapp">*Aplikacja</Label>
               <Input type="select" name="clientaddapp" id="clientaddapp" onChange={this.handle_clientaddappChange} disabled={this.state.disabledInputs.clientaddapp} required>
                    {allAppsArray}
               </Input>
          </FormGroup>



          <FormGroup>
               <Label for="clientaddappversion">*Wersja aplikacji</Label>
               <Input type="select" name="clientaddappversion" id="clientaddappversion" onChange={this.handle_clientaddappversionChange} disabled={this.state.disabledInputs.clientaddappversion} required>
                    {allAppsVersionsArray}
               </Input>
          </FormGroup>


          <FormGroup>
               <Label for="implementdate">*Data implementacji</Label>
               <Input type="date" name="implementdate" id="implementdate" disabled={this.state.disabledInputs.implementdate} required max="9999-12-31" />
          </FormGroup>

           <FormGroup>
               <Label for="desc">*Opis implementacji</Label>
               <Input type="textarea" name="desc" id="desc" disabled={this.state.disabledInputs.desc} required/>
          </FormGroup>


          <Row className="form-submit-row">
               <Col xs="4">
                    <Button size="lg" type="submit">Dodaj</Button>   
               </Col>    

               <Col xs="8">
                    <Alert isOpen={this.state.clientSuccessAddAppInfo} color="success">
                         Aplikacja klienta dodana.
                    </Alert>
               </Col>
           </Row> 

     </Form>



<Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

</section>
          
          )
     }
}