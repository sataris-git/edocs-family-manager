import React from 'react';
import { Button, Form, FormGroup, Label, Input, Row, Col, InputGroup, InputGroupAddon, InputGroupText, Alert } from 'reactstrap';

import axios from 'axios';
import { Error } from '../Error';


export class AppData extends React.Component {
     constructor(props) {
          super(props)

          this.state = { 

               appData: props.appData,
               editClientSuccessInfo: false,
               waitingForDownload: props.waitingForDownload,

               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }

     componentWillReceiveProps(nextProps) {
          //When recived new appData - update current using data
          this.setState({
               appData: nextProps.appData, 
               disableDeleteVersionButtons: false,
               waitingForDownload: nextProps.waitingForDownload
          })
     
     }

     handleChange = (e) => {

          let target = e.target;

               this.setState(() => {
                    let appData = this.state.appData;
                    appData[target.name] = target.value;
                    return (
                         {
                              appData: appData
                         }
                    )
          })
     }


     //After submit form
     handleSubmit = (e) => {
          e.preventDefault();
          
          axios.post('/edocs-family-manager/db/apps', {
               id: this.state.appData.id,
               name: this.state.appData.name,
               createDate: this.state.appData.createDate,
               appRank: this.state.appData.appRank,
               appDescription: this.state.appData.appsDescription
             })
             .then((response) => {
                    //Show success alert
                    this.setState({editClientSuccessInfo: true})
                    // this.props.downloadClientsList();
                    this.props.downloadAppsList();
                    //After 2000ms - reload client details table
                    setTimeout( () => {
                         this.setState({editClientSuccessInfo: false})
                    }, 2000);
             })
             .catch((error) => {
               this.setState({                    
                    error: {
                         display: true,
                         title: "Edycja informacji o aplikacji",
                         message: `Nie udało się edytować informacji o aplikacji`,
                         data: error.response.data
               }});
             });
     }
     
     render() {

          return (
               <section id="AppData">
               <h4>Dane aplikacji <br /> {this.state.appData.name} </h4>
               <Form onSubmit={this.handleSubmit} >
<Row>
     <Col>       
          <FormGroup>
               <Label for="name">*Nazwa aplikacji</Label>
               <InputGroup>
               <InputGroupAddon addonType="prepend"><InputGroupText>{this.state.appData.id}</InputGroupText></InputGroupAddon>
                    <Input type="text" name="name" id="name" value={this.state.appData.name} onChange={this.handleChange} disabled={this.state.waitingForDownload}  required/>
               </InputGroup>
          </FormGroup>

          <FormGroup>
               <Label for="createDate">*Data utworzenia</Label>
               <Input type="date" name="createDate" id="createDate" value={this.state.appData.createDate} onChange={this.handleChange}disabled={this.state.waitingForDownload} max="9999-12-31" required />
          </FormGroup>

          <FormGroup>
               <Label for="appRank">*Rząd aplikacji</Label>
               <Input type="number" name="appRank" id="appRank" value={this.state.appData.appRank} onChange={this.handleChange}disabled={this.state.waitingForDownload} min="2" max="3" required/>
          </FormGroup>

          <Button onClick={this.props.onDeleteClick} value={this.state.appData.id} disabled={this.state.waitingForDownload}  color="danger">Usuń</Button>   
     </Col>

     <Col>
          <FormGroup className="appsDescription">
               <Label for="appsDescription">*Opis</Label>
               <Input type="textarea" name="appsDescription" id="appsDescription" value={this.state.appData.appsDescription} onChange={this.handleChange} disabled={this.state.waitingForDownload}  required/>
          </FormGroup>
     </Col>
</Row>

<Row>
     <Col>          
          <Button color="warning" type="submit" disabled={this.state.waitingForDownload} >Zapisz zmiany</Button>  
          <Alert  isOpen={this.state.editClientSuccessInfo} color="success">
               Zmiany zostały zapisane
          </Alert>
     </Col>
</Row>



</Form>

<Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

               </section>
          )
     }
}