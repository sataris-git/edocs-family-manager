/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.DAO;

import java.util.List;

/**
 *
 * @author John
 */
public class DependencyReaderDAO {

        private String groupId;
        private String artifactId;
        private String version;
        
        private List<DependencyReader_DependencyListDAO> dependencies;

        
        
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<DependencyReader_DependencyListDAO> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<DependencyReader_DependencyListDAO> dependencies) {
        this.dependencies = dependencies;
    }

        
        
    

        
        
        
    
}
