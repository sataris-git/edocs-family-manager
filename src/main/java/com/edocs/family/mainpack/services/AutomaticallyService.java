/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.services;

import com.edocs.family.mainpack.DAO.DependencyReaderDAO;
import org.springframework.stereotype.Service;

/**
 *
 * @author John
 */


@Service
public class AutomaticallyService {
    
    
    public void addAppsAndDependencies(DependencyReaderDAO csharpRequest) {
        
//        csharpRequest.getGroupId();
//        csharpRequest.getArtifactId();
//        csharpRequest.getVersion();
//        csharpRequest.getDependencyListAll();
        

        System.out.println("####################");
        System.out.println(csharpRequest.getGroupId());
        System.out.println(csharpRequest.getArtifactId());
        System.out.println(csharpRequest.getVersion());
        System.out.println("-------------");
        
        
        csharpRequest.getDependencies().forEach( (i) -> { 
        
            System.out.println(i.getGroupId());
            System.out.println(i.getArtifactId());
            System.out.println(i.getVersion());
            System.out.println("~~");
        } );
        
        System.out.println("####################");
        
    }
    
}
