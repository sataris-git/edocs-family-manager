/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author John
 */
@Entity
@Table(name = "dependencies")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dependencies.findAll", query = "SELECT d FROM Dependencies d")
    , @NamedQuery(name = "Dependencies.findById", query = "SELECT d FROM Dependencies d WHERE d.id = :id")})
public class Dependencies implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "child_app_version_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private AppsVersions childAppVersionId;
    @JoinColumn(name = "parent_app_version_id", referencedColumnName = "id")
    @ManyToOne
    @NotNull
    private AppsVersions parentAppVersionId;

    public Dependencies() {
    }

    public Dependencies(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AppsVersions getChildAppVersionId() {
        return childAppVersionId;
    }

    public void setChildAppVersionId(AppsVersions childAppVersionId) {
        this.childAppVersionId = childAppVersionId;
    }

    public AppsVersions getParentAppVersionId() {
        return parentAppVersionId;
    }

    public void setParentAppVersionId(AppsVersions parentAppVersionId) {
        this.parentAppVersionId = parentAppVersionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dependencies)) {
            return false;
        }
        Dependencies other = (Dependencies) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.edocs.family.mainpack.entities.Dependencies[ id=" + id + " ]";
    }
    
}
