/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.services;


import com.edocs.family.DTO.AppsAllTO;
import com.edocs.family.DTO.ClientsAppsTO;
import com.edocs.family.DTO.ClientsImplementHistoryTO;
import com.edocs.family.DTO.ClientsInfoTO;
import com.edocs.family.mainpack.DAO.ClientAppHistoryDAO;
import com.edocs.family.mainpack.entities.ClientsApps;
import com.edocs.family.mainpack.entities.ClientsInfo;
import com.edocs.family.mainpack.repository.ClientsAppsRepository;
import com.edocs.family.mainpack.repository.ClientsInfoRepository;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



/**
 *
 * @author John
 */

@Service
public class ClientsService {
    
    @Autowired
    public ClientsAppsRepository clientsAppsRepository;
     
    @Autowired
    public ClientsInfoRepository clientsInfoRepository;
    
    @Autowired
    public AppsService appsService;
            

    public ClientAppHistoryDAO clientAppHistoryDAO = new ClientAppHistoryDAO();
    
    
    
    
    public List<ClientsInfo> findAllClients() {
        List<ClientsInfo> dtoList = new ArrayList<>();
        
        clientsInfoRepository.findAll().forEach((i) -> {
            dtoList.add(i);
        });
        
        dtoList.sort(Comparator.comparing(ClientsInfo::getId));
        return dtoList;
    }
    
    
            
    public ClientsInfoTO findByid(int id) {
        ModelMapper modelMapper = new ModelMapper();
        
        ClientsInfoTO dto = modelMapper.map(clientsInfoRepository.findByid(id), ClientsInfoTO.class);
        
        
        List<ClientsAppsTO> dtoList = new ArrayList<>();
        
        clientsAppsRepository.findByclientId(id).forEach((i) -> {
            ClientsAppsTO dto2 = modelMapper.map(i, ClientsAppsTO.class);
            dtoList.add(dto2);
         });

        dto.setClientApps(dtoList);
        
        return dto;
    }
    
    

    public Iterable<ClientsApps> findAll() {

        return clientsAppsRepository.findAll();
    }
    
    
    
    public List<ClientsAppsTO> findByclientId(int id) {
        
        ModelMapper modelMapper = new ModelMapper();
        List<ClientsAppsTO> dtoList = new ArrayList<>();
        
        clientsAppsRepository.findByclientId(id).forEach((i) -> {
            ClientsAppsTO dto = modelMapper.map(i, ClientsAppsTO.class);
            dtoList.add(dto);
         });
  
        return dtoList;
    }
    
    
    
    public Map<Integer, AppsAllTO> clientUsingApps(int id) {

        Set<Integer> usingApps = this.clientUsingAppsIdList(id);
        
        List<AppsAllTO> listApps = new ArrayList<>();
        
        Map<Integer, AppsAllTO> mapdto = new HashMap<>();
        
        usingApps.forEach((i) -> { 
           
            mapdto.put(i, appsService.findByid(i));
                                 
        });

        return mapdto;
    }
    
    
    
    
    public Set<Integer> clientUsingAppsIdList(int id) {
        
        List<Integer> listToReturn = new ArrayList<>();
        
        clientsAppsRepository.findByclientId(id).forEach((i) -> {
                
            listToReturn.add(i.getAppVersionId().getAppId().getId());
            
         });
        
        Set<Integer> listToReturnUnique = new HashSet<Integer>(listToReturn);
        
        return listToReturnUnique;
    }

    @Transactional
    public void setClientAppVersion(int clientAppsId, int appVerId ){
        clientsAppsRepository.setClientAppVersion(clientAppsId, appVerId);
    }
     
    public List<ClientsImplementHistoryTO> clientAppsHistory(ClientAppHistoryDAO clientAppHistoryDAO) {
        
        int id = clientAppHistoryDAO.getClientId();
        
        ModelMapper modelMapper = new ModelMapper();
        
        List<ClientsImplementHistoryTO> dtoList = new ArrayList<>();
        
        clientsAppsRepository.findByclientId(id).forEach((i) -> {
            if (i.getAppVersionId().getAppId().getId() == clientAppHistoryDAO.getAppId()) {
                ClientsImplementHistoryTO dto = modelMapper.map(i, ClientsImplementHistoryTO.class);
                dtoList.add(dto);
            } 
         });
        
        dtoList.sort(Comparator.comparing(ClientsImplementHistoryTO::getImplementDate).reversed());

        return dtoList;
    }
    
}

