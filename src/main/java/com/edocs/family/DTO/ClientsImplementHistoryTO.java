/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.DTO;

import com.edocs.family.mainpack.entities.AppsVersions;
import java.util.Date;


/**
 *
 * @author John
 */
public class ClientsImplementHistoryTO {
    
    private Integer id;
    private String version;
    private String implementDate;
    private String disableDate;
    private String implementDescription;
    private AppsVersions appVersionId;
 
    
    public String getVersion() {
        return appVersionId.getVersion();
    }

    public void setVersion(String version) {
        this.version = version;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImplementDate() {
        return implementDate;
    }

    public void setImplementDate(String implementDate) {
        this.implementDate = implementDate;
    }

    public String getDisableDate() {
        return disableDate;
    }

    public void setDisableDate(String disableDate) {
        this.disableDate = disableDate;
    }

    public String getImplementDescription() {
        return implementDescription;
    }

    public void setImplementDescription(String implementDescription) {
        this.implementDescription = implementDescription;
    }

    public Integer getAppVersionId() {
        return appVersionId.getId();
    }

    public void setAppVersionId(AppsVersions appVersionId) {
        this.appVersionId = appVersionId;
    }

}
