package com.edocs.family.mainpack.repository;


import com.edocs.family.mainpack.entities.Apps;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AppsRepository extends JpaRepository<Apps, Integer> {
 
    public Apps findByid(int id);

}
