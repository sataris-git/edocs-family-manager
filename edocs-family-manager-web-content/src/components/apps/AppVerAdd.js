import React from 'react';
import axios from 'axios';

import { Button, Form, FormGroup, Label, Input, Row, Col, Alert } from 'reactstrap';
import { Error } from '../Error';

export class AppVerAdd extends React.Component {
     constructor(props) {
          super(props)

          this.state = {
               appsList: props.appsList,
               appSuccessAddVerInfo: false,
               appSelected: false,
               appIdDisabled: true,
               inputsDisabled: true,
               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }


     componentDidMount() {
          this.componentWillReceiveProps(this.props);
          this.props.appearAnimate("#AppVerAdd");
     }

     componentWillReceiveProps(nextProps) {
          //Set results when recived new props
          this.setState((prevState) => {

               let appIdDisabled = prevState.appIdDisabled;

               if(Array.isArray(nextProps.appsList))
               appIdDisabled = false;
               

               return ({
                    appIdDisabled: appIdDisabled,
                    appsList: nextProps.appsList
               })
          });
     }

     
     handle_appIdChange = () => {
          this.setState({ 
               appSelected: true, 
               inputsDisabled: false
           });
     }


     //After submit form
     handleSubmit = (e) => {
          e.preventDefault();

          let form = e.target;

          //Disable "ADD" button, prevent double add 
          form.querySelector('button').setAttribute("disabled", true)

          axios.post('/edocs-family-manager/db/apps/versions', {
               appId: parseInt(form.appId.value, 10),
               version: form.version.value,
               creationDate: form.creationDate.value,
               changelog: form.changelog.value
             })
             .then((responseClientAddApp) => {
                    //If all is ok
                    if(responseClientAddApp.status === 200) {
                         this.appSuccessAddVerInfo();
                         this.props.downloadAppsList();
                         //Reset inputs in form, and enable "ADD" button
                         form.reset();
                         form.querySelector('button').removeAttribute("disabled");
                    }
             })
             .catch((error) => {
               this.setState({                    
                    error: {
                         display: true,
                         title: "Dodawanie aplikacji klienta",
                         message: "Nie udało się dodać aplikacji klienta do bazy danych.",
                         data: error.response.data
               }});
             });
     }



     appSuccessAddVerInfo() {
          this.setState({
               appSuccessAddVerInfo: true,
 
               appSelected: false,
               inputsDisabled: true
          })

          setTimeout( () => {
               this.setState({appSuccessAddVerInfo: false})
          }, 2000);
     }


     render() {
          let allAppsArray = <option>{this.state.appsList}</option>;
          if(Array.isArray(this.state.appsList) ) {
               allAppsArray = this.state.appsList.map((element) =>
                    <option key={element.id} value={element.id}>{element.name}</option>
               );
               allAppsArray.unshift(<option key='default' value="" disabled={this.state.appSelected}>-- Wybierz aplikacje --</option>)
          }
          
          return (
<section id="AppVerAdd">
     <h3>Dodawanie wersji aplikacji</h3>
     

     <Form onSubmit={this.handleSubmit}>

          <FormGroup>
               <Label for="appId">*Dla aplikacji</Label>
               <Input type="select" name="appId" id="appId" disabled={this.state.appIdDisabled} onChange={this.handle_appIdChange} required>
                    {allAppsArray}
               </Input>
          </FormGroup>

          <FormGroup>
               <Label for="version">*Wersja</Label>
               <Input type="text" name="version" id="version" placeholder="x.x.x.x" disabled={this.state.inputsDisabled} required/>
          </FormGroup>

          <FormGroup>
               <Label for="creationDate">*Data utworzenia</Label>
               <Input type="date" name="creationDate" id="creationDate" disabled={this.state.inputsDisabled} required max="9999-12-31" />
          </FormGroup>

           <FormGroup>
               <Label for="changelog">*Opis zmian</Label>
               <Input type="textarea" name="changelog" id="changelog" disabled={this.state.inputsDisabled} required/>
          </FormGroup>

          <Row className="form-submit-row">
               <Col xs="4">
                    <Button size="lg" type="submit">Dodaj</Button>   
               </Col>    

               <Col xs="8">
                    <Alert isOpen={this.state.appSuccessAddVerInfo} color="success">
                         Aplikacja klienta dodana.
                    </Alert>
               </Col>
           </Row> 

     </Form>



<Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

</section>
          
          )
     }
}