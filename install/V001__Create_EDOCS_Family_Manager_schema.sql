DROP SCHEMA IF EXISTS family_manager CASCADE;
CREATE SCHEMA family_manager;



CREATE SEQUENCE family_manager.apps_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "family_manager"."apps" (
    "id" integer DEFAULT nextval('family_manager.apps_id_seq') NOT NULL,
    "app_description" character varying(255) NOT NULL,
    "app_rank" integer,
    "create_date" date NOT NULL,
    "name" character varying(50),
    CONSTRAINT "apps_pkey" PRIMARY KEY ("id")
) WITH (oids = false);



CREATE SEQUENCE family_manager.apps_versions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "family_manager"."apps_versions" (
    "id" integer DEFAULT nextval('family_manager.apps_versions_id_seq') NOT NULL,
    "app_id" integer NOT NULL,
    "version" character varying(16) NOT NULL,
    "creation_date" date NOT NULL,
    "changelog" text NOT NULL,
    CONSTRAINT "apps_versions_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "apps_versions_app_id_fkey" FOREIGN KEY (app_id) REFERENCES family_manager.apps(id) NOT DEFERRABLE
) WITH (oids = false);



CREATE SEQUENCE family_manager.clients_info_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "family_manager"."clients_info" (
    "id" integer DEFAULT nextval('family_manager.clients_info_id_seq') NOT NULL,
    "companyname" character varying(50) NOT NULL,
    "join_date" date NOT NULL,
    "patron" character varying(64) NOT NULL,
    "description" text NOT NULL,
    CONSTRAINT "clients_info_pkey" PRIMARY KEY ("id")
) WITH (oids = false);



CREATE SEQUENCE family_manager.clients_apps_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "family_manager"."clients_apps" (
    "id" integer DEFAULT nextval('family_manager.clients_apps_id_seq') NOT NULL,
    "client_id" integer NOT NULL,
    "implement_date" date NOT NULL,
    "disable_date" date,
    "app_version_id" integer NOT NULL,
    "implement_description" text,
    CONSTRAINT "clients_apps_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "clients_apps_app_version_id_fkey" FOREIGN KEY (app_version_id) REFERENCES family_manager.apps_versions(id) NOT DEFERRABLE,
    CONSTRAINT "clients_apps_client_id_fkey" FOREIGN KEY (client_id) REFERENCES family_manager.clients_info(id) NOT DEFERRABLE
) WITH (oids = false);



CREATE SEQUENCE family_manager.dependencies_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "family_manager"."dependencies" (
    "id" integer DEFAULT nextval('family_manager.dependencies_id_seq') NOT NULL,
    "parent_app_version_id" integer NOT NULL,
    "child_app_version_id" integer NOT NULL,
    CONSTRAINT "dependencies_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "dependencies_child_app_version_id_fkey" FOREIGN KEY (child_app_version_id) REFERENCES family_manager.apps_versions(id) NOT DEFERRABLE,
    CONSTRAINT "dependencies_parent_app_version_id_fkey" FOREIGN KEY (parent_app_version_id) REFERENCES family_manager.apps_versions(id) NOT DEFERRABLE
) WITH (oids = false);