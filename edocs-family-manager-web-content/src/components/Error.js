/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import '../css/Error.css';

export class Error extends React.Component {
     constructor(props) {
          super(props);
          this.state = {
               modal: true    
          };

          this.toggle = this.toggle.bind(this);
     }

     toggle() {
          this.setState({
               modal: !this.state.modal
          });

          setTimeout(() => { window.location.reload(); }, 300)
     }

     render() {

          if(this.props.display === false) {
               return null;
          }

          
          let errordata = []
          
          //Preventing display error message when data is object
          //Making better view of error data display
          if (typeof (this.props.data) === "object") {

               for (let key in this.props.data) {

               let cell1 = key;
               let cell2 = this.props.data[key];

               errordata.push(<tr key={key}><td>{cell1}</td><td>{cell2}</td></tr>)

               }

          } else {
               errordata.push(<tr> <tr> {this.props.data} </tr> </tr>)
          }



          return (
               <div>
               <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
               <ModalHeader toggle={this.toggle}><span className="error-title">Błąd: </span>{this.props.title}</ModalHeader>
               <ModalBody >
                    <div className="error-message">{this.props.message}</div>
                    <hr />
                    <table className="error-data">
                    <tbody>
                         <tr> 
                              <th colSpan="2">Error:</th> 
                         </tr>
                         
                              {errordata}
                         </tbody>
                    </table>
               </ModalBody>
               <ModalFooter>
                    <Button color="primary" onClick={this.toggle}>Przeładuj strone</Button>{' '}
               </ModalFooter>
               </Modal>
               </div>
          );
     }
}
