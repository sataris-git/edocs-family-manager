/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.DTO;

import java.util.Date;
import java.util.List;

/**
 *
 * @author John
 */
public class ClientsInfoTO {
    
    private Integer id;
    private String companyname;
    private String joinDate;
    private String patron;
    private String description;
    private List<ClientsAppsTO> clientApps;

    
    
    public List<ClientsAppsTO> getClientApps() {
        return clientApps;
    }

    public void setClientApps(List<ClientsAppsTO> clientApps) {
        this.clientApps = clientApps;
    }
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getPatron() {
        return patron;
    }

    public void setPatron(String patron) {
        this.patron = patron;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
