import React, { Component } from 'react';
import axios from 'axios';

import { Container, Row, Col, Button } from 'reactstrap';
import { Route, NavLink } from "react-router-dom";
import FontAwesome from 'react-fontawesome';

// Main components
import { Error } from './Error';

// App components
import { AppAddForm } from './apps/AppAddForm';
import { AppVerAdd } from './apps/AppVerAdd';

import { AppManage } from './apps/AppManage';

export class AppsMain extends Component {
     constructor(props) {
          super(props);

          this.state = {
               appsList: "... pobieranie danych",
               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          };
     }


     componentDidMount() {
          this._mounted = true;
          this.props.appearAnimate("#AppsMain");
     }



     componentWillUnmount() {
     this._mounted = false;
     }
        

     componentWillMount() {
          this.downloadAppsList();
     }


     //Tab toggler from Add/Edit
     toggle = (tab) => {
          if (this.state.activeTab !== tab) {
               this.setState({
                    activeTab: tab
               });
          }
     }


     //Function sended as callback - downloading all apps list
     downloadAppsList = () => {
          //Load apps data
          axios.get(`/edocs-family-manager/db/apps`)
          .then((resultGetAppsList) => {

                    if(this._mounted === true ) {
                         //If after ask for get apps data, there is any changes - set new state (else: nothing to do)
                         if(JSON.stringify(resultGetAppsList.data) !== JSON.stringify(this.state.appsList) ) {
                              this.setState({ 
                                   appsList: resultGetAppsList.data,
                              })
                         }
                    }


               })
               .catch((error) => {

                    if(this._mounted === true ) {

                         this.setState({                    
                              error: {
                                   display: true,
                                   title: "Pobieranie listy aplikacji",
                                   message: "Nie udało się pobrać listy aplikacji.",
                                   data: error.response.data
                         }});

                    }
          });
     }


     render() {

          return (

          <Container id="AppsMain" className="admin-container">
               <Row className="head-row">
                    <Col className="head-title"><h2>Aplikacje</h2></Col>
                    <Col className="head-buttons">
                         <Row>
                              <Col xs="12">                          

                                        <NavLink exact to={this.props.match.path} activeClassName="btn-active">
                                             <Button className="button-add" ><FontAwesome name='plus-circle' /> Dodawanie</Button>
                                        </NavLink>

                                        <NavLink to={this.props.match.path + "/edit"} activeClassName="btn-active">
                                             <Button className="button-edit"  ><FontAwesome name='edit' /> Edycja</Button>
                                        </NavLink>


                              </Col>
                         </Row>
                    </Col>
               </Row>

               <Route exact path={this.props.match.path} component={this.appAdd} />
               <Route path={this.props.match.path + "/edit"} component={this.appEdit} />

               <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

          </Container>
          );
     }


     appAdd = () => {

          return ( 

               <Row className="content-row-adds">
                    <Col xs="12" md="6"><AppAddForm appearAnimate={this.props.appearAnimate} downloadAppsList={this.downloadAppsList}/></Col>
                    <Col xs="12" md="6"><AppVerAdd appearAnimate={this.props.appearAnimate} downloadAppsList={this.downloadAppsList} appsList={this.state.appsList} /></Col>
               </Row>

          );
     }


     appEdit = () => {

          return ( 

               <Row>
                    <Col><AppManage appearAnimate={this.props.appearAnimate} downloadAppsList={this.downloadAppsList} appsList={this.state.appsList} /></Col>
               </Row>
               
          );
     }



}

