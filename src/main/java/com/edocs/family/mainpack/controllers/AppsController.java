/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.controllers;

import com.edocs.family.DTO.AppsAllTO;
import com.edocs.family.mainpack.entities.Apps;
import com.edocs.family.mainpack.services.AppsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PS
 */

@RestController
@RequestMapping("/db/apps")
public class AppsController {
 
    @Autowired
    public AppsService appsService;
    
    //----- GET methods
    
    //Return all apps in data base
    @RequestMapping(method = RequestMethod.GET)
    public List<AppsAllTO> listReturning() {
        return appsService.findAll();
    }
   
    //Returns details about app what id is {id}
    @RequestMapping(value = "/{id}",
                    method = RequestMethod.GET,
                    produces = "application/json")
    public AppsAllTO listReturningwithId(@PathVariable("id") int id) {
        return appsService.findByid(id);
    }
    
    
    
    //----- POST methods
    //Add new app or edit existing one 
    @RequestMapping(method = RequestMethod.POST)
    public void addApp(@RequestBody Apps apps) {
        appsService.addApp(apps);
    }
    
    
    
    //----- DELETE methods    
    //Delete app
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) 
    public void deleteApp(@PathVariable int id) {
        appsService.appDelete(id); 
    }
    
    
    
}
