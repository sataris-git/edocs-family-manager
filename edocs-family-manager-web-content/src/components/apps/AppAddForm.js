import React from 'react';
import axios from 'axios';

import { Button, Form, FormGroup, Label, Input, Alert, Row, Col } from 'reactstrap';
import { Error } from '../Error';

export class AppAddForm extends React.Component {

     constructor(props) {
          super(props)

          this.state = {
               appSuccessAddInfo: false,
               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }
     

     componentDidMount() {
          this.props.appearAnimate("#AppAddForm");
     }

     

     handleSubmit = (e) => {
          e.preventDefault();

          let form = e.target;

          //Disable "ADD" button, prevent double add 
          form.querySelector('button').setAttribute("disabled", true)

          axios.post('/edocs-family-manager/db/apps', {
               name: form.name.value,
               createDate: form.createDate.value,
               appRank: form.appRank.value,
               appDescription: form.appDescription.value
               })
               .then((responseAppAdd) => {
                    //If all is ok
                    if(responseAppAdd.status === 200) {
                         this.props.downloadAppsList();
                         this.appSuccessAddInfo();
                         //Reset inputs in form, and enable "ADD" button
                         form.reset();
                         form.querySelector('button').removeAttribute("disabled");
                    }
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Dodawanie aplikacji",
                              message: "Nie udało się dodać aplikacji do bazy danych.",
                              data: error.response.data
                    }});
               });
     }

     

     appSuccessAddInfo() {
          this.setState({appSuccessAddInfo: true})
          setTimeout( () => {
               this.setState({appSuccessAddInfo: false})
          }, 2000);
     }




     render() {

          return (
          <section id="AppAddForm">
               <h3>Dodawanie aplikacji</h3>
               <Form onSubmit={this.handleSubmit} >
                    <FormGroup>
                         <Label for="name">*Nazwa aplikacji</Label>
                         <Input type="text" name="name" id="name" placeholder="...nazwa aplikacji" required/>
                    </FormGroup>

                    <FormGroup>
                         <Label for="createDate">*Data utworzenia</Label>
                         <Input type="date" name="createDate" id="createDate" max="9999-12-31" required />
                    </FormGroup>

                    <FormGroup>
                         <Label for="appRank">*Rząd aplikacji</Label>
                         <Input type="number" name="appRank" id="appRank" min="2" max="3" step="1" placeholder="2 lub 3" required />
                    </FormGroup>

                    <FormGroup>
                         <Label for="appDescription">*Opis</Label>
                         <Input type="textarea" name="appDescription" id="appDescription" required/>
                    </FormGroup>


                    <Row className="form-submit-row">
                         <Col xs="4">
                              <Button size="lg" type="submit">Dodaj</Button>   
                         </Col>    

                         <Col xs="8">
                              <Alert isOpen={this.state.appSuccessAddInfo} color="success">
                                   Aplikacja została dodana.
                              </Alert>
                         </Col>
                    </Row> 
               </Form>

          <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

          </section>

          )
     }
}


