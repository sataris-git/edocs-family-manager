import React from 'react';
import axios from 'axios';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

import { AppsTable } from './AppsTable';
import { AppData } from './AppData';
import { AppVerManage } from './AppVerManage';

import { Error } from '../Error';

export class AppManage extends React.Component {
     constructor(props) {
          super(props)
    
          this.state = {
               appsList: props.appsList,
               appEdit: null,

               activeTab: '2',
               appData: null,
               waitingForDownload: false,

               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }


     componentDidMount() {
          this.props.appearAnimate("#AppManage");
     }

     componentWillReceiveProps(nextProps) {
          //Set results when recived new props
          this.setState({
               appsList: nextProps.appsList,
               waitingForDownload: false
               })

          //When user already edit any app, and there is any new info about it (like new versions)
          if(this.state.appData !== null) {
               nextProps.appsList.forEach(element => {
                    if(element.id === this.state.appData.id) {
                         this.setState({
                              appData: element
                              })
                    }
               });
          }
     }

     //After "EDIT" button click
     handleEditButtonClick = (e) => {

          this.setState({
               appEdit: e.target.value,
          })

          this.downloadAppData(e.target.value);


     }


     downloadAppData = (id) => {
          this.setState({ 
               waitingForDownload: true
          })
          axios.get(`/edocs-family-manager/db/apps/${id}`)
          .then((resultGetAppData) => {
                    //When new appData - update current using data
                    if(JSON.stringify(this.state.appData) !== JSON.stringify(resultGetAppData.data) ) {
                         this.setState({
                              appData: {
                                   id: resultGetAppData.data.id,
                                   name: resultGetAppData.data.name,
                                   createDate: resultGetAppData.data.createDate,
                                   appRank: resultGetAppData.data.appRank,
                                   appsDescription: resultGetAppData.data.appsDescription,
                                   versions: resultGetAppData.data.versions
                              }
                         })
                    }

                    this.setState({ 
                         waitingForDownload: false
                    })
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie informacji o aplikacji",
                              message: `Nie udało się pobrać informacji o aplikacji`,
                              data: error.response.data
                    }});
          });
     }


     //After "DELETE" button click
     handleDeleteButtonClick = (e) => {
          if (window.confirm("Jesteś pewien?")) {
               //Prevent double click on "DELETE" button
               this.setState({
                    appData: null,
                    waitingForDownload: true
               })

               axios.delete(`/edocs-family-manager/db/apps/${e.target.value}`, {})
               .then((response) => {
                         //Get new, updated client list
                         this.props.downloadAppsList();
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Usuwanie aplikacji z bazy danych",
                              message: `Nie udało się usunąć aplikacji z bazy danych. Upewnij się, czy do aplikacji nie ma przypisanych żadnych wersji aplikacji!`,
                              data: error.response.data
                    }});
               });

          }
     }


     toggle = (tab) => {
          if (this.state.activeTab !== tab) {
               this.setState({
                    activeTab: tab
               });
          }

     }


     appEdit() {
          if(this.state.appData !== null) {
               return(
                    <div>
                         <Nav className="tabs" tabs>
                              <NavItem>
                                   <NavLink
                                        className={classnames({ active: this.state.activeTab === '1' })}
                                        onClick={() => { this.toggle('1'); }}
                                        >
                                        Dane aplikacji 
                                   </NavLink>
                              </NavItem>
                              <NavItem>
                                   <NavLink
                                        className={classnames({ active: this.state.activeTab === '2' })}
                                        onClick={() => { this.toggle('2'); }}
                                        >
                                        Wersje aplikacji
                                   </NavLink>
                              </NavItem>
                         </Nav>

                         <TabContent activeTab={this.state.activeTab}>
                              <TabPane tabId="1">

                                   <AppData downloadAppsList={this.props.downloadAppsList} 
                                   waitingForDownload={this.state.waitingForDownload} 
                                   appData={this.state.appData} 
                                   onDeleteClick={this.handleDeleteButtonClick} 
                                   />

                              </TabPane>

                              <TabPane tabId="2">
                                   <AppVerManage 
                                   waitingForDownload={this.state.waitingForDownload} 
                                   downloadAppData={this.downloadAppData} 
                                   appData={this.state.appData} 
                                   appEdit={this.state.appEdit} 
                                   appsList={this.state.appsList}
                                   />

                              </TabPane>
                         </TabContent>
                    </div>
               ) 
          }
          return null;
     }


     render() {
          return (
          <section id="AppManage">
               <h3>Menadżer aplikacji</h3>

               <section>
                    <AppsTable waitingForDownload={this.state.waitingForDownload} appsList={this.state.appsList} onDeleteClick={this.handleDeleteButtonClick} onEditClick={this.handleEditButtonClick} appEdit={this.state.appEdit} />
               </section>

               {this.appEdit()}

               <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />
          
          </section>
          )
     }
}