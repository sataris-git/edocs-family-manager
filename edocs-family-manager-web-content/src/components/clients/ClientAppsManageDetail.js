import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button, Table, Alert } from 'reactstrap'
import axios from 'axios';


export class ClientAppsManageDetail extends Component {
     constructor(props) {
          super(props);

          this.state = {
               detailsEdit: props.detailsEdit,

               versionList: props.versionList,

               appVersionId: props.detailsEdit.appVersionId,
               implementDate: props.detailsEdit.implementDate,
               implementDescription: props.detailsEdit.implementDescription,
               disableDate: props.detailsEdit.disableDate,

               radioVersionValue: props.detailsEdit.appVersionId.id,

               editVersionSuccessInfo: false,

               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }

          }

        }

        

     componentWillReceiveProps(nextProps) {
          if(this.props.detailsEdit !== nextProps.detailsEdit) {
               this.setState( { 
                    detailsEdit: nextProps.detailsEdit,

                    versionList: nextProps.versionList,

                    appVersionId: nextProps.detailsEdit.appVersionId,
                    implementDate: nextProps.detailsEdit.implementDate,
                    implementDescription: nextProps.detailsEdit.implementDescription,
                    disableDate: nextProps.detailsEdit.disableDate,

                    radioVersionValue: nextProps.detailsEdit.appVersionId.id
               })
          }
     }


     handleChange = (e) => {
               this.setState({[e.target.name]: e.target.value
          })
     }



     handleSubmit = (e) => {
          e.preventDefault();

          axios.post('/edocs-family-manager/db/clients/addclientapp', {
               id: this.props.detailsEdit.id,
               clientId: parseInt(this.props.clientId, 10),
               implementDate: e.target.implementDate.value,
               disableDate: e.target.disableDate.value,
               implementDescription: e.target.implementDescription.value,
               appVersionId: parseInt(e.target.radioVersionValue.value, 10)
          })
          .then((response) => {

               //Show success alert
               this.setState({editVersionSuccessInfo: true})

               //After 2000ms - reload client details table
               setTimeout( () => {
                    this.setState({editVersionSuccessInfo: false})
                    this.props.downloadClientData();
               }, 2000);

          })
          .catch((error) => {
               this.setState({                    
                    error: {
                         display: true,
                         title: "Edycja informacji o wersji aplikacji",
                         message: `Nie udało się edytować informacji o wersji aplikacji`,
                         data: error.response.data
               }});
          });
     }
     




     render() {

          let versions = this.state.versionList.map( (i) => {

          //Set radio when current version = actual iterate version on table
               let checkedval;
               if(parseInt(this.state.radioVersionValue, 10) === i.id) {
                    checkedval = true
                    } else {
                    checkedval = false; 
               }

               return (
                    <tr key={i.id}>
                         <th scope="row">{i.version}</th>
                         <td>{i.creationDate}</td>
                         <td>{i.changelog}</td>
                         <td>
                              <FormGroup check>
                                   <Label check>
                                        <Input type="radio" name="radioVersionValue" value={i.id} onChange={this.handleChange} checked={checkedval} />
                                   </Label>
                              </FormGroup>
                         </td>
                         </tr>
               )    
          })

          //Prevent from display "Warning" note, when value = null - it must be empty string
               let disableDateValue = this.state.disableDate;
               if(this.state.disableDate === null) {
                    disableDateValue = "";
               }

               let implementDescriptionValue = this.state.implementDescription;
               if(this.state.implementDescription === null) {
                    implementDescriptionValue = "";
               }

     return (
          <section id="ClientAppsManageDetail">
               <h5>Edycja szczegółów aplikacji klienta<br /> <strong>{this.state.appVersionId.appId.name} </strong> ( {this.state.detailsEdit.appVersionId.version} )</h5>

               <Form className="client-data-form" onSubmit={this.handleSubmit}>
                    <Row>
                         <Col>
                              <FormGroup>
                                   <Label for="implementDate">Data implementacji:</Label>
                                   <Input type="date" name="implementDate" id="implementDate" value={this.state.implementDate} onChange={this.handleChange} required />
                              </FormGroup>

                              <FormGroup>
                                   <Label for="implementDescription"> Opis implementacji:</Label>
                                   <Input type="textarea" name="implementDescription" id="implementDescription" value={implementDescriptionValue} onChange={this.handleChange}  required />
                              </FormGroup>

                              <FormGroup>
                                   <Label for="disableDate">Data wyłączenia wsparcia:</Label>
                                   <Input type="date" name="disableDate" id="disableDate" placeholder="date placeholder" value={disableDateValue} onChange={this.handleChange} />
                              </FormGroup>

                              <Row>
                                   <Col sm="8">
                                        <Button color="warning">Zapisz zmiany</Button>
                                   </Col>
                                   
                                   <Col sm="4">
                                        <Button color="danger" onClick={ () => { this.props.handleVersionDeleteCallback(this.props.detailsEdit.id) } }>Usuń</Button>
                                   </Col>
                              </Row>

                              
                              <Row>
                                   <Col>
                                        <Alert  isOpen={this.state.editVersionSuccessInfo} color="success">
                                             Zmiany zostały zapisane
                                        </Alert>
                                   </Col>
                              </Row>


                         </Col>
                         <Col>
                              <Table striped>
                              <thead>
                                   <tr>
                                   <th>Wersja</th>
                                   <th>Data utworzenia</th>
                                   <th>Changelog</th>
                                   <th>Ustaw</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   {versions}
                              </tbody>
                              </Table>
                         </Col>
                    </Row>    
               </Form>




          </section>

          );
     }


}

