import React, { Component } from 'react';
import axios from 'axios';
import { Container, Row, Col, Button } from 'reactstrap';
import { Route, NavLink } from "react-router-dom";
import FontAwesome from 'react-fontawesome';
import { Error } from './Error';

// Client components
import { ClientAddForm } from './clients/ClientAddForm';
import { ClientAppAdd } from './clients/ClientAppAdd';
import { ClientManage } from './clients/ClientManage';

export class ClientsMain extends Component {
     constructor(props) {
          super(props);

          this.state = {
               clientsList: "... pobieranie danych",
               appsList: "... pobieranie danych",
               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          };
     }


     componentDidMount() { 
       this._mounted = true;

       this.props.appearAnimate("#ClientsMain");

     }
     
     componentWillUnmount() {
       this._mounted = false;
     }
     
  


     componentWillMount() {
          this.downloadClientsList();
          this.downloadAppsList();
     }


     //Tab toggler from Add/Edit
     toggle = (tab) => {
          if (this.state.activeTab !== tab) {
               this.setState({
                    activeTab: tab
               });
          }
     }

     //Function sended as callback - downloading all clients list
     downloadClientsList = () => {
          //Load clients data
          axios.get(`/edocs-family-manager/db/clients`)
          .then((resultGetClientsList) => {

               if(this._mounted === true ) {
                    //If after ask for get clients data, there is any changes - set new state (else: nothing to do)
                    if(JSON.stringify(resultGetClientsList.data) !== JSON.stringify(this.state.clientsList) ) {
                         this.setState({ 
                              clientsList: resultGetClientsList.data,
                         })
                    }
               }


               })
               .catch((error) => {

                    if(this._mounted === true ) {
                         this.setState({                    
                              error: {
                                   display: true,
                                   title: "Pobieranie listy klientów",
                                   message: "Nie udało się pobrać listy klientów.",
                                   data: error.response.data
                         }});
                    }
          });
          
     }



     //Function sended as callback - downloading all apps list
     downloadAppsList = () => {

          //Load apps data
          axios.get(`/edocs-family-manager/db/apps`)
          .then((resultGetAppsList) => {

               if(this._mounted === true ) {
                    //If after ask for get apps data, there is any changes - set new state (else: nothing to do)
                    if(JSON.stringify(resultGetAppsList.data) !== JSON.stringify(this.state.appsList) ) {
                         this.setState({ 
                              appsList: resultGetAppsList.data,
                         })
                    }
               }


               })
               .catch((error) => {

               //Set state only if component is mouted!
               if(this._mounted === true ) {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie listy aplikacji",
                              message: "Nie udało się pobrać listy aplikacji.",
                              data: error.response.data
                    }});
               }

          });
     }


     render() {

          return (

          <Container id="ClientsMain" className="admin-container">
               <Row className="head-row">
                    <Col className="head-title"><h2>Klienci</h2></Col>
                    <Col className="head-buttons">
                         <Row>
                              <Col xs="12">                          
                                        <NavLink exact to={this.props.match.path} activeClassName="btn-active">
                                             <Button className="button-add" ><FontAwesome name='user-plus' /> Dodawanie</Button>
                                        </NavLink>

                                        <NavLink to={this.props.match.path + "/edit"} activeClassName="btn-active">
                                             <Button className="button-edit"  ><FontAwesome name='edit' /> Edycja</Button>
                                        </NavLink>
                              </Col>
                         </Row>
                    </Col>
               </Row>


               <Route exact path={this.props.match.path} component={this.clientAdd} />
               <Route path={this.props.match.path + "/edit"} component={this.clientEdit} />

               <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

          </Container>
          );
     }


     clientAdd = () => {

          return ( 

               <Row className="content-row-adds">
                    <Col xs="12" md="6"><ClientAddForm appearAnimate={this.props.appearAnimate} downloadClientsList={this.downloadClientsList}/></Col>
                    <Col xs="12" md="6"><ClientAppAdd appearAnimate={this.props.appearAnimate} clientsList={this.state.clientsList} appsList={this.state.appsList} /></Col>
               </Row>

          );
     }


     clientEdit = () => {

          return ( 

               <Row>
                    <Col><ClientManage appearAnimate={this.props.appearAnimate} downloadClientsList={this.downloadClientsList} clientsList={this.state.clientsList} /></Col>
               </Row>
               
          );
     }

     

}

