/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.repository;

/**
 *
 * @author John
 */


import com.edocs.family.mainpack.entities.ClientsApps;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface ClientsAppsRepository extends CrudRepository<ClientsApps, Integer> {
    
    public ClientsApps findByid(int id);
    
    @Query("select c from ClientsApps c where c.clientId.id = :id")
    List<ClientsApps> findByclientId(@Param("id") int id);
    
    
    @Modifying
    @Query("update ClientsApps c set c.appVersionId.id = ?2 where c.id = ?1")
    int setClientAppVersion(int clientAppsId, int appVerId );

}