/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.services;

import com.edocs.family.DTO.AppVersionsAllTO;
import com.edocs.family.DTO.AppVersionsDetailsTO;
import com.edocs.family.DTO.AppVersionsWithoutAppIdTO;
import com.edocs.family.mainpack.entities.AppsVersions;
import com.edocs.family.mainpack.repository.AppsVersionsRepository;
import com.edocs.family.mainpack.repository.DependenciesRepository;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author PS
 */


@Service
public class AppsVersionsService {
    
    
    
    @Autowired
    public AppsVersionsRepository appsVersionsRepository;
        
    @Autowired
    public DependenciesService dependenciesService;
    
    @Autowired
    public DependenciesRepository dependenciesRepository;
    
        
    
    public List<AppVersionsAllTO> findAll() {
        
        ModelMapper modelMapper = new ModelMapper();

        List<AppVersionsAllTO> dtoList = new ArrayList<>();

        appsVersionsRepository.findAll().forEach((i) -> {
            
            AppVersionsAllTO dto = modelMapper.map(i, AppVersionsAllTO.class);

            dtoList.add(dto);
        });
        
        
        return dtoList;
    }
    
    
   
    
    public AppVersionsDetailsTO findByid(int id) {
        
        ModelMapper modelMapper = new ModelMapper();

        AppVersionsDetailsTO dto = modelMapper.map(appsVersionsRepository.findByid(id), AppVersionsDetailsTO.class);

        return dto;
    }

    
    
    
    
    public List<AppsVersions> sortedfindByappId(int id) {
        
        List<AppsVersions> dtoList = new ArrayList<>();
        
        appsVersionsRepository.findByappId(id).forEach((i) -> {
            dtoList.add(i);
        });
        
        dtoList.sort(Comparator.comparing(AppsVersions::getCreationDate).reversed());
        
        return dtoList;
    }
    
    
    public List<AppVersionsWithoutAppIdTO> findByappId(int id) {
        
        ModelMapper modelMapper = new ModelMapper();
        
        List<AppVersionsWithoutAppIdTO> dtoList = new ArrayList<>();

        this.sortedfindByappId(id).forEach((i) -> {
            
            AppVersionsWithoutAppIdTO dto = modelMapper.map(i, AppVersionsWithoutAppIdTO.class);
            
            dtoList.add(dto);
        });
        
        return dtoList;
    }
    
    
    
    public void addAppVersion(AppsVersions appsVersions) {
        appsVersionsRepository.save(appsVersions);
    } 
    
    
    
    public void appVersionDelete(int id) {
        appsVersionsRepository.delete(id);
    } 
    
    
    
    
}
