/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.controllers;

import com.edocs.family.DTO.AppVersionsAllTO;
import com.edocs.family.DTO.AppVersionsDetailsTO;
import com.edocs.family.mainpack.entities.AppsVersions;
import com.edocs.family.mainpack.repository.AppsVersionsRepository;
import com.edocs.family.mainpack.services.AppsVersionsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PS
 */

@RestController
@RequestMapping("/db/apps/versions")
public class AppsVersionsController {
 
    
    
    @Autowired
    public AppsVersionsRepository appsVersionsRepository;
    
    
    @Autowired
    public AppsVersionsService appsVersionsService;

    
    //----- GET methods
    
    //Return all versions of all apps
    @RequestMapping(method = RequestMethod.GET)
    public List<AppVersionsAllTO> findAll() {

        return appsVersionsService.findAll();
    }
    
    //Returns details about app version what id is {id}
    @RequestMapping(value = "/{id}",
                    method = RequestMethod.GET,
                    produces = "application/json")
    public AppVersionsDetailsTO findByid(@PathVariable("id") int id) {
        return appsVersionsService.findByid(id);
    }
    
    
    //Returns list of app versions assigned to app what id is {id}
    @RequestMapping(value = "/app/{id}",
                    method = RequestMethod.GET,
                    produces = "application/json")
    public List<AppsVersions> findByappId(@PathVariable("id") int id) {
        return appsVersionsRepository.findByappId(id);
    }
    
    


    //----- POST methods
    
    //Add new app version
    @RequestMapping(method = RequestMethod.POST)
    public void addAppVersion(@RequestBody AppsVersions appsVersions) {
        appsVersionsService.addAppVersion(appsVersions);
    }
    


    
    //----- DELETE methods    
    //Delete app version
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) 
    public void deleteAppVersion (@PathVariable int id) {
        appsVersionsService.appVersionDelete(id); 
    }
    
    
    

}
