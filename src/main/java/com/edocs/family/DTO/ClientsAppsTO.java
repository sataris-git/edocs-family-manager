/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.DTO;

import com.edocs.family.mainpack.entities.AppsVersions;
import java.util.Date;

/**
 *
 * @author John
 */
public class ClientsAppsTO {
    
    private Integer id;
    private String implementDate;
    private String disableDate;
    private String implementDescription;
    private AppsVersions appVersionId;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImplementDate() {
        return implementDate;
    }

    public void setImplementDate(String implementDate) {
        this.implementDate = implementDate;
    }

    public String getDisableDate() {
        return disableDate;
    }

    public void setDisableDate(String disableDate) {
        this.disableDate = disableDate;
    }

    public String getImplementDescription() {
        return implementDescription;
    }

    public void setImplementDescription(String implementDescription) {
        this.implementDescription = implementDescription;
    }

    public AppsVersions getAppVersionId() {
        return appVersionId;
    }

    public void setAppVersionId(AppsVersions appVersionId) {
        this.appVersionId = appVersionId;
    }

    
    
}
