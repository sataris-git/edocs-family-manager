import React from 'react';
import axios from 'axios';

import { Button, Form, FormGroup, Label, Input, Alert, Row, Col } from 'reactstrap';
import { Error } from '../Error';

export class ClientAddForm extends React.Component {

     constructor(props) {
          super(props)

          this.state = {
               clientSuccessAddInfo: false,
               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }

     } 


     componentDidMount() { 
          this.props.appearAnimate("#ClientAddForm");
     }

     handleSubmit = (e) => {
          e.preventDefault();

          let form = e.target;

          //Disable "ADD" button, prevent double add 
          form.querySelector('button').setAttribute("disabled", true)
          
          let clientName = form.clientname.value;
          let joinDate = form.joindate.value;
          let patron = form.patron.value;
          let desc = form.desc.value;

          axios.post('/edocs-family-manager/db/clients', {
                    companyname: clientName,
                    joinDate: joinDate,
                    patron: patron,
                    description: desc
               })
               .then((responseClientAdd) => {
                    //If all is ok
                    if(responseClientAdd.status === 200) {
                         this.props.downloadClientsList();
                         this.clientSuccessAddInfo();
                         //Reset inputs in form, and enable "ADD" button
                         form.reset();
                         form.querySelector('button').removeAttribute("disabled");
                    }
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Dodawanie klienta",
                              message: "Nie udało się dodać klienta do bazy danych.",
                              data: error.response.data
                    }});
               });
     }


     clientSuccessAddInfo() {
          this.setState({clientSuccessAddInfo: true})
          setTimeout( () => {
               this.setState({clientSuccessAddInfo: false})
          }, 2000);
     }




     render() {

          return (
<section id="ClientAddForm">
     <h3>Dodawanie klienta</h3>
     <Form onSubmit={this.handleSubmit} >
          <FormGroup>
               <Label for="clientname">*Nazwa klienta</Label>
               <Input type="text" name="clientname" id="clientname" placeholder="...nazwa klienta" required/>
          </FormGroup>

          <FormGroup>
               <Label for="joindate">*Data dołączenia</Label>
               <Input type="date" name="joindate" id="joindate" required max="9999-12-31" />
          </FormGroup>

          <FormGroup>
               <Label for="patron">*Patron</Label>
               <Input type="text" name="patron" id="patron" placeholder="...nazwa patrona klienta"  required/>
          </FormGroup>

           <FormGroup>
               <Label for="desc">*Opis</Label>
               <Input type="textarea" name="desc" id="desc"  required/>
          </FormGroup>


          <Row className="form-submit-row">
               <Col xs="4">
                    <Button size="lg" type="submit">Dodaj</Button>   
               </Col>   

               <Col xs="8">
                    <Alert isOpen={this.state.clientSuccessAddInfo} color="success">
                              Użytkownik został dodany.
                    </Alert>
               </Col>
           </Row> 
     </Form>

<Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

</section>

          )
     }
}


