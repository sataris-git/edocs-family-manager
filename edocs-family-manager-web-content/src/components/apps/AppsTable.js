import React from 'react';
import { Table, Button } from 'reactstrap';
import { Loading } from '../Loading'

export class AppsTable extends React.Component {
     constructor(props) {
          super(props)

          this.state = { 
               appsList: props.appsList,
               waitingForDownload: props.waitingForDownload,
               appEdit: props.appEdit
          }
     }

     componentWillReceiveProps(nextProps) {
          //Set results when recived new props
          this.setState({
                    appsList: nextProps.appsList,
                    waitingForDownload: nextProps.waitingForDownload,
                    appEdit: nextProps.appEdit
               })
     }


     render() {
          let tableContent = <tr><td style={{textAlign: 'center'}} colSpan="4"><Loading /></td></tr>;

          //When LOADED.
          if(Array.isArray(this.state.appsList) ) {
               tableContent = this.state.appsList.map((element) => { 
               
                    let selectedRow;
                    if(parseInt(this.state.appEdit, 10) === parseInt(element.id, 10) ) {
                         selectedRow = "selected"
                    }

                    return (
                         <tr key={element.id} className={selectedRow} >
                              <th scope="row">{element.id}</th>
                              <td>{element.name}</td>
                              <td><Button color="info"   onClick={this.props.onEditClick} value={element.id} disabled={this.state.waitingForDownload} >Edytuj</Button></td>
                              <td><Button color="danger" onClick={this.props.onDeleteClick} value={element.id} disabled={this.state.waitingForDownload} >Usuń</Button></td>
                         </tr>
                    )});
          }


          return (
               <Table striped id="AppsTable">
                    <thead>
                         <tr>
                              <th>#</th>
                              <th>Nazwa</th>
                              <th>Akcja</th>
                              <th></th>
                         </tr>
                    </thead>
                    <tbody>
                         {tableContent}
                    </tbody>
               </Table>
          )
     }
}