import React from "react";
import { HashRouter, BrowserRouter, Route, NavLink, Link } from "react-router-dom";

import { Collapse, Navbar, NavbarToggler, Nav, NavItem, Button } from 'reactstrap';
import { AppsAndClients } from './AppsAndClients';
import { AdminPanel } from './components/AdminPanel';
import FontAwesome from 'react-fontawesome';


import 'bootstrap/dist/css/bootstrap.css';

import './css/Main.css';

import './css/Apps.css';
import './css/Clients.css';


export class Main extends React.Component {

     constructor(props) { 
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
     this.setState({
       isOpen: !this.state.isOpen
     });
   }



     render() {
     
          return (
               <section className="MainContainer">

     <HashRouter>
     {/* <BrowserRouter basename="/edocs-family-manager"> */}


               <div>

                    <Navbar className="main-nav" style={ {top: 0 } } color="faded" light expand="md">
                    <Link className="navbar-brand" to={'/'}>

                         <h1>EDOCS Family Manager </h1>
                    
                    </Link>
                    <NavbarToggler onClick={this.toggle} />
                         <Collapse isOpen={this.state.isOpen} navbar>
                              <Nav className="ml-auto" navbar>
                              
                                   <NavItem>
                                        <NavLink exact to="/" className="nav-link" activeClassName="main-nav-element-active">
                                    
                                             <Button size="lg"><FontAwesome name='align-left' /> Display</Button>
                                             
                                        </NavLink>
                                   </NavItem>
                                   <NavItem>
                                        <NavLink to="/admin" className="nav-link" activeClassName="main-nav-element-active">
                                             <Button size="lg"> <FontAwesome name='adn' /> Admin</Button>
                                        
                                             
                                        </NavLink>
                                   </NavItem>
                              </Nav>
                         </Collapse>
                    </Navbar>


                    <Route exact path="/" component={AppsAndClients} />
                    <Route path="/admin" component={AdminPanel} />

               </div>
               
     {/* </BrowserRouter> */}

     </HashRouter>



               
               </section>
             )
     }

}




