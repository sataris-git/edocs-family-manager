import React from "react";
import { Route, Link, NavLink } from "react-router-dom";
import { ClientsMain } from './ClientsMain';
import { AppsMain } from './AppsMain';
import { Navbar, NavbarToggler, Collapse, Nav, NavItem, Button } from 'reactstrap'
import FontAwesome from 'react-fontawesome';

export class AdminPanel extends React.Component {
     constructor(props) {
          super(props)

          this.state = {
            isOpen: false
          };
     }

     componentDidMount() {
          this.appearAnimate("#AdminPanel");
     }


     appearAnimate(stringId) {
          document.querySelector(stringId).style.opacity = 0;
          document.querySelector(stringId).style.transition = "0.5s";

          setTimeout( () => {

               if(document.querySelector(stringId) !== null ) {
                    document.querySelector(stringId).style.opacity = 1;
               }

               setTimeout( () => {
                    if(document.querySelector(stringId) !== null ) {
                         document.querySelector(stringId).style.transition = null;
                    }
               }, 1000 );

          }, 100 );
     }



     toggle = () => {
          this.setState({
               isOpen: !this.state.isOpen
          });
     }


     HomePage() {

          return <article className="home-page">
<h2>EDOCS Family Manager</h2> 
     <div>
Jest aplikacją slużącą do podglądu wszystkich dostępnych w bazie danych klientów oraz 
aplikacji firmy <strong>Żbik Sp. z o.o.</strong>, wraz z ich zależnościami. Dodatkowo można sprawdzać dowolne szegóły implementacji danych wersji aplikacji u konkretnych klientów, oraz szczegóły dotyczące samych klientów. Istotną możliwością, jest także możliwość podglądu zależności wersji aplikacji w jej szczegółach.
     </div>

     <hr />

<h2>EDOCS Family Manager - Admin Panel</h2> 

     <div>
Jest częścią aplikacji <strong>EDOCS Family Manager</strong> slużącą do zarządzania bazą danych klientów oraz aplikacji firmy <strong>Żbik Sp. z o.o.</strong>.</div>
<div style={{fontStyle: "italic"}}>Za jej pomocą możemy zmieniać dane dla następujących kategorii:</div>

<br />

<div>
     <h3>Klienci:</h3>
     <ul> 
          <li>Dodawać nowych klientów (wraz z datą, patronem, opisem klienta),</li>
          <li>Przypisywać aplikacje do klientów (wraz z datą i opisem implementacji),</li>
          <li>Edytować wcześniej dodane dane użytkownika, oraz jego aplikacje,</li>
          <li>Usuwać użytkowników z bazy danych lub/i przypisane do niego aplikacje</li>
     </ul>
     <hr style={{width: 70+"%"}} />

     <h3>Aplikacje:</h3>
     <ul> 
          <li>Dodawać nowe aplikacje (wraz z datą, rzędem, opisem),</li>
          <li>Przypisywać wersje do aplikacje (wraz z datą i opisem implementacji),</li>
          <li>Edytować wcześniej dodane dane aplikacji, oraz jej wersje,</li>
          <li>Edytować i dodawać nowe zależności do różnych wersji aplikacji,</li>
          <li>Mieć kompletny podgląd na zależności wersji aplikacji, oraz dodatkowo aplikacji zależnych od danej wersji,</li>
          <li>Usuwać aplikacje z bazy danych lub/i przypisane do niej wersje</li>
     </ul>
</div>

<hr />

<div style={ { textDecoration: "underline" } } >
     Pełna dokumentacja techniczna dostępna jest na stronie: <a href="https://docs.google.com/document/d/1m6wd5Ju1pYFPRa0snd-oQ-ja7WJ_YhUoE5x3YGtsIUc/edit?ts=5a97f3f0">dokumentacja EDOCS Family Manager</a>
</div>

          </article>;
     }

     NavBar = () => (
          <Navbar color="faded" light expand="md">
               <Link className="navbar-brand" to={this.props.match.path}>
                  
                         EDOCS Family Manager - Admin Panel
                    
               </Link>
          <NavbarToggler onClick={(this.toggle)} />
          <Collapse isOpen={this.state.isOpen} navbar>
               <Nav className="ml-auto" navbar>
                    <NavItem>
                         <NavLink to={this.props.match.path + "/clients"} className="nav-link" activeClassName="admin-nav-element-active">
                         <Button color="secondary"><FontAwesome name='users' /> Klienci</Button>
                         </NavLink>
                    </NavItem>
                    <NavItem>
                         <NavLink to={this.props.match.path + "/apps"} className="nav-link" activeClassName="admin-nav-element-active">
                         <Button color="secondary"><FontAwesome name='cubes' /> Aplikacje</Button>
                         </NavLink>
                    </NavItem>
               </Nav>
          </Collapse>
          </Navbar>
     );


     render() {
          return (
                  <section id="AdminPanel">
                       {this.NavBar()}

                       <hr />

                         <Route exact path={this.props.match.path} component={this.HomePage} />

                         <Route
                              path={this.props.match.path + "/clients"}
                              render={(props) => <ClientsMain {...props} appearAnimate={this.appearAnimate} />}
                         />

                         <Route
                              path={this.props.match.path + "/apps"}
                              render={(props) => <AppsMain {...props} appearAnimate={this.appearAnimate} />}
                         />

                  
                  </section>
             )
     }

}






