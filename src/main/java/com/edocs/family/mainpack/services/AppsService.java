/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.services;

import com.edocs.family.DTO.AppsAllTO;
import com.edocs.family.mainpack.entities.Apps;
import com.edocs.family.mainpack.repository.AppsRepository;
import com.edocs.family.mainpack.repository.AppsVersionsRepository;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



/**
 *
 * @author John
 */

@Service
public class AppsService {
    
    
    
    @Autowired
    public AppsRepository appsRepository;
     
    
    @Autowired
    public AppsVersionsRepository appsVersionsRepository;
     
            
    @Autowired
    public AppsVersionsService appsVersionsService;
    

    public AppsAllTO findByid(int id) {

        ModelMapper modelMapper = new ModelMapper();
        
        AppsAllTO dto = modelMapper.map(appsRepository.findByid(id), AppsAllTO.class);

        dto.setVersions(appsVersionsService.findByappId(dto.getId()));
                
        
        
        return dto;
  
    }

    
    
    
    
    public List<AppsAllTO> findAll() {

            ModelMapper modelMapper = new ModelMapper();

            List<AppsAllTO> dtoList = new ArrayList<>();

            appsRepository.findAll().forEach((i) -> {
               
                AppsAllTO dto = modelMapper.map(i, AppsAllTO.class);
                
                dto.setVersions(appsVersionsService.findByappId(dto.getId()));

                dtoList.add(dto);
                
            });
        
            
        dtoList.sort(Comparator.comparing(AppsAllTO::getId));
            
            
        return dtoList;
    }
    

    
    
    public void addApp(Apps apps) {
        appsRepository.save(apps);
    } 
    

    
    public void appDelete(int id) {
        appsRepository.delete(id);
    } 
    
   
}
