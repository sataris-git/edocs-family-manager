package com.edocs.family.mainpack.repository;


import com.edocs.family.mainpack.entities.AppsVersions;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Repository;


@Repository
public interface AppsVersionsRepository extends JpaRepository<AppsVersions, Integer> {
 
    
    public AppsVersions findByid(int id);

   
    @Query("select c from AppsVersions c where c.appId.id = :id")
    List<AppsVersions> findByappId(@Param("id") int id);

}
