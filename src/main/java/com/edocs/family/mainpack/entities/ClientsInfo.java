/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author John
 */
@Entity
@Table(name = "clients_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClientsInfo.findAll", query = "SELECT c FROM ClientsInfo c")
    , @NamedQuery(name = "ClientsInfo.findById", query = "SELECT c FROM ClientsInfo c WHERE c.id = :id")
    , @NamedQuery(name = "ClientsInfo.findByCompanyname", query = "SELECT c FROM ClientsInfo c WHERE c.companyname = :companyname")
    , @NamedQuery(name = "ClientsInfo.findByJoinDate", query = "SELECT c FROM ClientsInfo c WHERE c.joinDate = :joinDate")
    , @NamedQuery(name = "ClientsInfo.findByPatron", query = "SELECT c FROM ClientsInfo c WHERE c.patron = :patron")
    , @NamedQuery(name = "ClientsInfo.findByDescription", query = "SELECT c FROM ClientsInfo c WHERE c.description = :description")})
public class ClientsInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "companyname")
    private String companyname;
    @Basic(optional = false)
    @NotNull
    @Column(name = "join_date")
    @Temporal(TemporalType.DATE)
    private Date joinDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "patron")
    private String patron;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "description")
    private String description;


    public ClientsInfo() {
    }

    public ClientsInfo(Integer id) {
        this.id = id;
    }

    public ClientsInfo(Integer id, String companyname, Date joinDate, String patron, String description) {
        this.id = id;
        this.companyname = companyname;
        this.joinDate = joinDate;
        this.patron = patron;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public String getPatron() {
        return patron;
    }

    public void setPatron(String patron) {
        this.patron = patron;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientsInfo)) {
            return false;
        }
        ClientsInfo other = (ClientsInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.edocs.family.mainpack.entities.ClientsInfo[ id=" + id + " ]";
    }
    
}
