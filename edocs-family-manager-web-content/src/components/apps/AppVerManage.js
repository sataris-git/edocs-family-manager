import React from 'react';
import { Table, Button } from 'reactstrap';
import axios from 'axios';
import { AppManageDetail } from './AppManageDetail';
import { Error } from '../Error';

export class AppVerManage extends React.Component {
     constructor(props) {
          super(props)

          this.state = { 
               appsList: props.appsList,

               appData: props.appData,

               detailsEdit: null,

               waitingForDownload: props.waitingForDownload,

               disableChangeVersionButtons: false,
               disableDeleteVersionButtons: false,

               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }

     
     co
     componentWillReceiveProps(nextProps) {
          //When recived new appData - update current using data
          this.setState({
               appData: nextProps.appData, 
               disableDeleteVersionButtons: false,
               waitingForDownload: nextProps.waitingForDownload
          })

          //If user click at edit on another app than current 
          if(this.state.appData.id !== nextProps.appData.id) {
               //Disable detailsEdit table show
               this.setState({
                    detailsEdit: null
               })
          }
     }


     //After click "DELETE" button
     handleVersionDelete = (id) => {

          if (window.confirm("Jesteś pewien?")) {
               //Blocking possibility to click "delete version button"
               this.setState({
                              waitingForDownload: true,
                              detailsEdit: null
                         })

               axios.delete(`/edocs-family-manager/db/apps/versions/${id}`)
               .then((response) => {
                              //Enabling possibility to click "delete version button" & download current data
                              this.props.downloadAppData(this.state.appData.id);
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Usunięcie wersji aplikacji",
                              message: `Nie udało się usunąć wersji aplikacji. Upewnij się, że do tej wersji nie są przypisane żadne zależności! (Lub sama nie jest zależnością!)`,
                              data: error.response.data
                    }});
               });
          }
     }


     //After click "EDIT DETAILS" button
     handleEditDetails = (detailsEdit) => {

          let detailsEdit_extended = detailsEdit;
          detailsEdit_extended.appId = this.state.appData.id;

          this.setState( {
               detailsEdit: detailsEdit_extended,
          })
     
     }



     //All table data to display
     tabledata() {

          let tabledata;
          //When get data
          if(this.state.appData.versions.length > 0 ) {   

               tabledata = this.state.appData.versions.map( (i) => {
                    //Select current editing row
                    let selectedRow;

                    if (this.state.detailsEdit !== null && 'id' in this.state.detailsEdit) {
                         if(this.state.detailsEdit.id === i.id)
                         selectedRow = "selected";
                    }

               return (
               <tr className={selectedRow} key={i.id}>
                    <th scope="row">{i.id}</th>
                    <td>{i.version}</td>
                    <td>{i.creationDate}</td>
                    <td><Button disabled={this.state.waitingForDownload} onClick={ () => {this.handleEditDetails(i) }} color="info">Edytuj szczegóły</Button></td>
                    <td><Button disabled={this.state.waitingForDownload} onClick={ () => { this.handleVersionDelete(i.id) } } color="danger">Usuń</Button></td>
               </tr>
               )} ) 


          } else {

               //When there is no any data
               tabledata =   <tr>
                    <th colSpan="5" scope="row">Brak danych</th>
               </tr>
          }


          return tabledata;
     }




     render() {
          let appVerManageDetail = null;
          if(this.state.detailsEdit !== null) {
               appVerManageDetail = <AppManageDetail 
                                        downloadAppData={this.props.downloadAppData} 
                                        detailsEdit={this.state.detailsEdit} 
                                        handleVersionDeleteCallback={this.handleVersionDelete}
                                        appsList={this.state.appsList}
                                   />
          } 

          return (
               <section id="AppVerManage">

               <h4>Wersje aplikacji <br /> {this.state.appData.name}</h4>
                    <Table striped>
                         <thead>
                              <tr>
                                   <th rowSpan="2">ID</th>
                                   <th rowSpan="2">Wersja</th>
                                   <th rowSpan="2">Data utworzenia</th>
                                   <th colSpan="2">Akcja</th>
                              </tr>
                              <tr>
                                   <th>Edytuj szczegóły</th>
                                   <th>Usuń</th>
                              </tr>
                         </thead>
                         <tbody>
                              {this.tabledata()}
                         </tbody>
                    </Table>

                    {appVerManageDetail}

                    <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />
               
               </section>
          )
     }
}