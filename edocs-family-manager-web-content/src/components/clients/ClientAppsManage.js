import React from 'react';
import { Table, Button, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import axios from 'axios';
import { ClientAppsManageDetail } from './ClientAppsManageDetail';
import { Loading } from '../Loading'
import { Error } from '../Error';


export class ClientAppsManage extends React.Component {
     constructor(props) {
          super(props)

          this.state = { 
               clientApps: "",
               clientUsingApps: "",
               dropdownOpen: {},
               detailsEdit: null,
               versionList: null,
               disableChangeVersionButtons: false,
               disableDeleteVersionButtons: false,
               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }


     componentWillMount() {
          this.downloadClientData(this.props);
          this.downloadUsingAppsData(this.props);
     }


     //When props changed, and there is any new props
     componentWillReceiveProps(nextProps) {
          if(this.props.clientId !== nextProps.clientId) {
               this.downloadClientData(nextProps);
               this.downloadUsingAppsData(nextProps);
          }
     }


     //Enabling Buttons in ClientTable component, when all data is loaded
     componentDidUpdate() {
          if (this.props.disableButtons === true &&  Array.isArray(this.state.clientApps) === true && typeof(this.state.clientUsingApps) === "object") {

               this.props.disableTableButtons(false)
          }
     }

     //Download clients data and disable rendering ClinetAppsManageDetail component
     downloadClientData(props) {
          this.setState({ 
               clientApps: "",
               detailsEdit: null
          })

          axios.get(`/edocs-family-manager/db/clients/${props.clientId}`)
          .then((result) => {
                    //When all client apps versions is loaded
                    this.setState({ 
                         clientApps: result.data.clientApps,
                    })
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie informacji o kliencie",
                              message: `Nie udało się pobrać informacji o kliencie`,
                              data: error.response.data
                    }});
          });
     }

     //Download map of using apps by client (to make version change dropdown list)
     downloadUsingAppsData(props) {
          this.setState({ 
               clientUsingApps: "",
          })

          axios.get(`/edocs-family-manager/db/clients/${props.clientId}/usingapps`)
          .then((result) => {
                    //When all client using apps versions is loaded
                    this.setState({ 
                         clientUsingApps: result.data,
                    })
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie używanych aplikacji przez klienta",
                              message: `Nie udało się pobrać używanych aplikacji przez klienta`,
                              data: error.response.data
                    }});
          });
     }


     //Everything what happend, if any dropdown list is expanded
     toggle = (e) => {
          let dropdownOpen = this.state.dropdownOpen;

          dropdownOpen[e.target.value] = dropdownOpen[e.target.value];

          //If user click anywhere all dropdowns folding, only if it is not a current dropdown
          for (let key in dropdownOpen) {
               if(key !== e.target.value)
               dropdownOpen[key] = false;
          }

          dropdownOpen[e.target.value] = !dropdownOpen[e.target.value];
          
          this.setState({dropdownOpen: dropdownOpen});
     }


     //After click other than the current one version - on dropdown version list
     handleVersionChange = (verId, id) => {


          if (window.confirm("Jesteś pewien?")) {

               //Blocking possibility to click "change version button"
               this.setState({disableChangeVersionButtons: true})

               axios.post(`/edocs_family_manager/db/clients/changeversion/${id}/${verId}`)
               .then((response) => {
                    //Enabling possibility to click "change version button" & download current data
                    this.setState({disableChangeVersionButtons: false})
                    this.downloadClientData(this.props);
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Zmiana wersji aplikacji użytkownika",
                              message: `Nie udało się zmienić wersji aplikacji użytkownika`,
                              data: error.response.data
                    }});
               });

          }

     }



     //After click "DELETE" button
     handleVersionDelete = (id) => {

          if (window.confirm("Jesteś pewien?")) {
          
               //Blocking possibility to click "delete version button"
               this.setState({disableDeleteVersionButtons: true})

               axios.delete(`/edocs-family-manager/db/clients/deleteversion/${id}`)
               .then((response) => {
                              //Enabling possibility to click "delete version button" & download current data
                              this.setState({disableDeleteVersionButtons: false})
                              this.downloadClientData(this.props);
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Usunięcie wersji aplikacji użytkownika",
                              message: `Nie udało się usunąć wersji aplikacji użytkownika`,
                              data: error.response.data
                    }});
               });

          }
     }


     //After click "EDIT DETAILS" button
     handleEditDetails = (detailsEdit, versionList) => {
          this.setState( {
               detailsEdit: detailsEdit,
               versionList: versionList
          })
     }


     //All table data to display
     tabledata() {
          let tabledata;
          //When load any data
          if ( Array.isArray(this.state.clientApps) === false || typeof(this.state.clientUsingApps) !== "object") {
          
               tabledata =    <tr>
                                   <th colSpan="5" scope="row"><Loading /></th>
                              </tr>
          } else {
               //When there is any OK data
               if(this.state.clientApps.length > 0 ) {   
                    tabledata = this.state.clientApps.map( (i) => {
                         let versionList = this.state.clientUsingApps[i.appVersionId.appId.id].versions;
                         let dropDownItems = this.state.clientUsingApps[i.appVersionId.appId.id].versions.map((el) => {

                              //When actually dropdown item = current client app version
                              if(i.appVersionId.version === el.version) {
                                   return( <DropdownItem disabled key={el.id}>{el.version}</DropdownItem> );
                              } else {
                                   return( <DropdownItem key={el.id} onClick={ () => {this.handleVersionChange(el.id, i.id) }} >{el.version}</DropdownItem> );
                              }
                         });

                         let editVersion =   <ButtonDropdown isOpen={this.state.dropdownOpen[i.id]} toggle={this.toggle}>
                                                  <DropdownToggle disabled={this.state.disableChangeVersionButtons} value={i.id} caret>
                                                       {i.appVersionId.version}
                                                  </DropdownToggle>
                                                  <DropdownMenu>
                                                       <DropdownItem header disabled >{i.appVersionId.version}</DropdownItem>
                                                       <DropdownItem divider/>
                                                       {dropDownItems}
                                                  </DropdownMenu>
                                             </ButtonDropdown>


                         //Select current editing row
                         let selectedRow;


                         if (this.state.detailsEdit !== null && 'id' in this.state.detailsEdit) {
                              if(this.state.detailsEdit.id === i.id)
                              selectedRow = "selected";
                         }


                         return (
                              <tr className={selectedRow} key={i.id}>
                                   <th scope="row">{i.appVersionId.appId.name}</th>
                                   <td>{i.appVersionId.version}</td>
                                   <td><Button onClick={ () => {this.handleEditDetails(i, versionList) }} color="info">Edytuj szczegóły</Button></td>
                                   <td>
                                        {editVersion}
                                   </td>
                                   <td><Button disabled={this.state.disableDeleteVersionButtons} onClick={ () => { this.handleVersionDelete(i.id) } } color="danger">Usuń</Button></td>
                              </tr>
                    )} ) 
               } else {
                    //When there is no any data
                    tabledata =   <tr>
                         <th colSpan="5" scope="row">Brak danych</th>
                    </tr>
               }
          }

          return tabledata;
     }




     render() {
          
          let clientAppsManageDetail = null;
          
          if(this.state.detailsEdit !== null) {
               clientAppsManageDetail = <ClientAppsManageDetail downloadClientData={() => {this.downloadClientData(this.props)}} versionList={this.state.versionList} detailsEdit={this.state.detailsEdit} handleVersionDeleteCallback={this.handleVersionDelete} clientApps={this.state.clientApps} clientId={this.props.clientId} />
          } 

          return (
               <section id="ClientAppsManage">

               <h4>Aplikacje klienta</h4>
                    <Table striped>
                         <thead>
                              <tr>
                                   <th rowSpan="2">Aplikacja</th>
                                   <th rowSpan="2">Wersja</th>
                                   <th colSpan="3">Akcja</th>
                              </tr>
                              <tr>
                                   <th>Edytuj szczegóły</th>
                                   <th>Zmień wersje</th>
                                   <th>Usuń</th>
                              </tr>
                         </thead>
                         <tbody>
                              {this.tabledata()}
                         </tbody>
                    </Table>

                    {clientAppsManageDetail}

                    <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />
               
               </section>
          )
     }
}