/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.DTO;

import java.util.List;

/**
 *
 * @author John
 */
public class AppsAllTO {
    
    private int id;
    private String name;
    private String createDate;
    private int appRank;
    private String appsDescription;
    
    private List<AppVersionsWithoutAppIdTO> versions;

    
    public List<AppVersionsWithoutAppIdTO> getVersions() {
        return versions;
    }

    public void setVersions(List<AppVersionsWithoutAppIdTO> versions) {
        this.versions = versions;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAppRank() {
        return appRank;
    }

    public void setAppRank(int appRank) {
        this.appRank = appRank;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getAppsDescription() {
        return appsDescription;
    }

    public void setAppsDescription(String appsDescription) {
        this.appsDescription = appsDescription;
    }
    
}
