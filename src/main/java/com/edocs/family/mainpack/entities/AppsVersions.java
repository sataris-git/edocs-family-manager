/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author John
 */
@Entity
@Table(name = "apps_versions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AppsVersions.findAll", query = "SELECT a FROM AppsVersions a")
    , @NamedQuery(name = "AppsVersions.findById", query = "SELECT a FROM AppsVersions a WHERE a.id = :id")
    , @NamedQuery(name = "AppsVersions.findByVersion", query = "SELECT a FROM AppsVersions a WHERE a.version = :version")
    , @NamedQuery(name = "AppsVersions.findByCreationDate", query = "SELECT a FROM AppsVersions a WHERE a.creationDate = :creationDate")
    , @NamedQuery(name = "AppsVersions.findByChangelog", query = "SELECT a FROM AppsVersions a WHERE a.changelog = :changelog")})
public class AppsVersions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 16)
    @NotNull
    @Column(name = "version")
    private String version;
    @Basic(optional = false)
    @NotNull
    @Column(name = "creation_date")
    @Temporal(value=TemporalType.DATE)
    private Date creationDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "changelog")
    private String changelog;
    @NotNull
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    @ManyToOne
    private Apps appId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "appVersionId")
    private Collection<ClientsApps> clientsAppsCollection;
    @OneToMany(mappedBy = "childAppVersionId")
    private Collection<Dependencies> dependenciesCollection;
    @OneToMany(mappedBy = "parentAppVersionId")
    private Collection<Dependencies> dependenciesCollection1;

    public AppsVersions() {
    }

    public AppsVersions(Integer id) {
        this.id = id;
    }

    public AppsVersions(Integer id, Date creationDate, String changelog) {
        this.id = id;
        this.creationDate = creationDate;
        this.changelog = changelog;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getChangelog() {
        return changelog;
    }

    public void setChangelog(String changelog) {
        this.changelog = changelog;
    }

    public Apps getAppId() {
        return appId;
    }

    public void setAppId(Apps appId) {
        this.appId = appId;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppsVersions)) {
            return false;
        }
        AppsVersions other = (AppsVersions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.edocs.family.mainpack.entities.AppsVersions[ id=" + id + " ]";
    }
    
}
