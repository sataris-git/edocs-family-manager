package com.edocs.family.mainpack.repository;


import com.edocs.family.mainpack.entities.Dependencies;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DependenciesRepository extends JpaRepository<Dependencies, Integer> {
 
    public List<Dependencies> findByid(int id);
    
    @Query("select c from Dependencies c where c.parentAppVersionId.id = :id")
    List<Dependencies> parentOfRank(@Param("id") int id);

    @Query("select c from Dependencies c where c.childAppVersionId.id = :id")
    List<Dependencies> childOf(@Param("id") int id);

    

}
