package com.edocs.family.mainpack.repository;


import com.edocs.family.mainpack.entities.ClientsInfo;
import org.springframework.data.repository.CrudRepository;

public interface ClientsInfoRepository extends  CrudRepository<ClientsInfo, Integer> {
    
    public ClientsInfo findByid(int id);
    
}
