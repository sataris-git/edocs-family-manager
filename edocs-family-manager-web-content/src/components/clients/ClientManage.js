import React from 'react';
import { ClientsTable } from './ClientsTable';
import { ClientData } from './ClientData';
import { ClientAppsManage } from './ClientAppsManage';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import axios from 'axios';
import { Error } from '../Error';

export class ClientManage extends React.Component {
     constructor(props) {
          super(props)
    

          this.state = {
               clientsList: props.clientsList,
               clientEdit: null,
               activeTab: '2',
               disableTableButtons: false,

               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }
     }



     componentDidMount() { 
          this.props.appearAnimate("#ClientManage");
     }


     
     componentWillReceiveProps(nextProps) {
          //Set results when recived new props
          this.setState({
                    clientsList: nextProps.clientsList,
                    clientEdit: null
               })
          if(nextProps.clientsList !== this.props.clientsList) {
               this.disableTableButtons(false);
          }
     }


     //After "EDIT" button click
     handleEditButtonClick = (e) => {
          this.disableTableButtons(true);

          this.setState({
               clientEdit: e.target.value,
          })
     }

     //Function sended as callback - this will disable or enable click buttons on Clients Table
     disableTableButtons = (bool) => {
          this.setState({
               disableTableButtons: bool
          })
     }
     

     //After "DELETE" button click
     handleDeleteButtonClick = (e) => {

          if (window.confirm("Jesteś pewien?")) {

               //Prevent double click on "DELETE" button
               this.setState({
                    clientEdit: null,
                    disableTableButtons: true
               })

               axios.delete(`/edocs-family-manager/db/clients/${e.target.value}`, {})
               .then((response) => {
                         //Get new, updated client list
                         this.props.downloadClientsList();
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Usuwanie klienta z bazy danych",
                              message: `Nie udało się usunąć klienta z bazy danych. Upewnij się, czy do klienta nie ma przypisanych żadnych aplikacji!`,
                              data: error.response.data
                    }});
               });



          }

     }


     toggle = (tab) => {
          if (this.state.activeTab !== tab) {
            this.setState({
              activeTab: tab
            });
          }

        }


     clientEdit() {
          if(this.state.clientEdit !== null) {
               return(
                    <div>
                         <Nav className="tabs" tabs>
                              <NavItem>
                                   <NavLink
                                        className={classnames({ active: this.state.activeTab === '1' })}
                                        onClick={() => { this.toggle('1'); }}
                                        >
                                        Dane klienta 
                                   </NavLink>
                              </NavItem>
                              <NavItem>
                                   <NavLink
                                        className={classnames({ active: this.state.activeTab === '2' })}
                                        onClick={() => { this.toggle('2'); }}
                                        >
                                        Aplikacje klienta
                                   </NavLink>
                              </NavItem>
                         </Nav>

                         <TabContent activeTab={this.state.activeTab}>
                              <TabPane tabId="1">
                                   <ClientData onDeleteClick={this.handleDeleteButtonClick} clientId={this.state.clientEdit} downloadClientsList={this.props.downloadClientsList}/>
                              </TabPane>
                              

                              <TabPane tabId="2">
                                   <ClientAppsManage disableButtons={this.state.disableTableButtons} disableTableButtons={this.disableTableButtons} clientId={this.state.clientEdit}/>
                              </TabPane>

                         </TabContent>
                    </div>
               ) 
          }
          return null;
     }


     render() {
          return (
          <section id="ClientManage">
               <h3>Menadżer klienta</h3>

               <section>
                    <ClientsTable disableButtons={this.state.disableTableButtons} clientsList={this.state.clientsList} onDeleteClick={this.handleDeleteButtonClick} onEditClick={this.handleEditButtonClick} clientEdit={this.state.clientEdit} />
               </section>

               {this.clientEdit()}

               <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />
          
          </section>
          )
     }
}