/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.edocs.family.mainpack.controllers;

import com.edocs.family.DTO.AppVersionsDependencies;
import com.edocs.family.DTO.AppVersionsMapDependenciesTO;
import com.edocs.family.mainpack.entities.Dependencies;
import com.edocs.family.mainpack.repository.DependenciesRepository;
import com.edocs.family.mainpack.services.DependenciesService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author PS
 */

@RestController
@RequestMapping("/db/dependencies")
public class DependenciesController {
 
    @Autowired
    public DependenciesRepository dependenciesRepository;
   
    @Autowired
    public DependenciesService dependenciesService;
    
    
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Dependencies> listReturning() {

        return dependenciesRepository.findAll();
    }
    
    
    //----- GET methods
    
    //Get object with a map format of all dependencies where key is dependency rank and value of that key is list of apps on which this relationship occurs.
    @RequestMapping(value="/{id}", 
                method = RequestMethod.GET)
    public Map<Integer, List<AppVersionsMapDependenciesTO>> mapDependencies(@PathVariable("id") int id) {
        return dependenciesService.mapDependencies(id);
    }
    
    
    
    
    
//    NIE UŻYWANE. Nie opisane w dokumentacji
//    //Get list of all dependency app versions of {id} app version
//    @RequestMapping(value="/getchildrens_obj/{id}", 
//                method = RequestMethod.GET)
//    public List<AppVersionsDependencies> getChildren(@PathVariable("id") int id) {
//        return dependenciesService.getChildrens(id);
//    }
//    
    
    
    
//    NIE UŻYWANE. Nie opisane w dokumentacji
//    //Get list of all dependency app versions of {id} app version
//    @RequestMapping(value="/getparents_obj/{id}", 
//                method = RequestMethod.GET)
//    public List<AppVersionsDependencies> getParents(@PathVariable("id") int id) {
//        return dependenciesService.getParents(id);
//    }
    
    
    
    
//    NIE UŻYWANE. Nie opisane w dokumentacji
//    //Get object with a map format of all dependencies where key is dependency rank and value of that key is list of apps on which this relationship occurs.
//    @RequestMapping(value="/getparents/{id}", 
//                method = RequestMethod.GET)
//    public List<Integer> getparents(@PathVariable("id") int id) {
//        return dependenciesService.childOf(id);
//    }
    
    
    
    
    
    //Get object with a map format of all dependencies where key is dependency rank and value of that key is list of apps on which this relationship occurs.
    @RequestMapping(value="/getchildrens/{id}", 
                method = RequestMethod.GET)
    public List<Integer> getchildrens(@PathVariable("id") int id) {
        return dependenciesService.parentOf(id);
    }
    
    
 
    
    
    //----- POST methods
    
    //Add new app version
    @RequestMapping(method = RequestMethod.POST)
    public void addDependency(@RequestBody Dependencies dependencies) {
        dependenciesService.addDependency(dependencies);
    }
    
    
    
    //----- DELETE methods
        
    //Delete client
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE) 
    public void deleteDependency(@PathVariable int id) {
        dependenciesService.deleteDependency(id); 
    }
    
   
    
}
