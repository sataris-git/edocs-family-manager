/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.edocs.family.DTO;

import java.util.Date;
import java.util.List;

/**
 *
 * @author PS
 */

public class AppVersionsWithoutAppIdTO {
    

    private Integer id;
    private String version;
    private String creationDate;
    private String changelog;

    
    public String getCreationDate() {      
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
    

    public String getChangelog() {
        return changelog;
    }

    public void setChangelog(String changelog) {
        this.changelog = changelog;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
}
