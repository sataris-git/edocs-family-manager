import React from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button, Table, Alert } from 'reactstrap'
import axios from 'axios';
import { Loading } from '../Loading';
import { Error } from '../Error';


export class AppManageDetail extends React.Component {
     constructor(props) {
          super(props);

          this.state = {
               detailsEdit: props.detailsEdit,

               parentOfList: [],
               
               appsList: props.appsList,
               app_versions: "* Wybierz aplikacje *",

               editVersionSuccessInfo: false,

               addDependencySuccessInfo: false,

               dependencies: null,

               waitingForAction: false,
          
               disabledAppVerInput: true,
               selectedAppVerId: null,

               error: {
                    display: false,
                    title: null,
                    message: null,
                    data: null
               }
          }

     }


     componentWillMount() {
          this.getDependencies(this.state.detailsEdit.id);
     }
     
     
     componentWillReceiveProps(nextProps) {
          if(this.props.detailsEdit !== nextProps.detailsEdit) {
               this.setState( { 
                    detailsEdit: nextProps.detailsEdit
               })

               this.getDependencies(nextProps.detailsEdit.id);
          }
     }



     getDependencies(id) {

          this.getparentOfInfo(id);

          this.setState( { 
               dependencies: null
          })

          axios.get(`/edocs-family-manager/db/dependencies/${id}`)
          .then((resultGetDependencies) => {
                    this.setState({ 
                         dependencies: resultGetDependencies.data
                    })
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie informacji o zależnościach",
                              message: `Nie udało się pobrać informacji o zależnościach`,
                              data: error.response.data
                    }});
          });
     }




     getparentOfInfo(id) {

          this.setState({ 
               parentOfList: []
          })

          axios.get(`/edocs-family-manager/db/dependencies/getchildrens/${id}`)
          .then((resultGetparentOfInfo) => {
                    this.setState({ 
                         parentOfList: resultGetparentOfInfo.data
                    })
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie informacji o zależnościach",
                              message: `Nie udało się pobrać informacji o zależnościach`,
                              data: error.response.data
                    }});
          });
     }




     //When any value chanded in app version data form
     handleChange = (e) => {
          let target = e.target;

               this.setState(() => {
                    let detailsEdit = this.state.detailsEdit;
                    detailsEdit[target.name] = target.value;
                    return (
                         {
                              detailsEdit: detailsEdit
                         }
                    )
          })
     }


     //When app is changed in dependencies form
     handle_appChange = (e) => {

          this.setState({
                    app_versions: "... pobieranie danych",
                    disabledAppVerInput: true,
                    selectedAppVerId: null
               })

          //Load app versions data
          axios.get(`/edocs-family-manager/db/apps/${e.target.value}`)
          .then((result) => {

                    this.setState((prevState) => {
                         return ({ 
                              app_versions: result.data.versions,
                              disabledAppVerInput: false
                         })
                    });
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Pobieranie wersji aplikacji",
                              message: `Nie udało się pobrać wersji aplikacji`,
                              data: error.response.data
                    }});
          });
     }



     //When app ver in select list is changed (Dependency form)
     handle_appVerChange = (e) => {
          this.setState({
               selectedAppVerId: e.target.value,
          })
     }



     //When submit data form of app version
     handleSubmitData = (e) => {
          e.preventDefault();

          this.setState({
               waitingForAction: true
          })

          axios.post('/edocs-family-manager/db/apps/versions', {
               id: this.state.detailsEdit.id,
               appId: this.state.detailsEdit.appId,
               version: this.state.detailsEdit.version,
               creationDate: this.state.detailsEdit.creationDate,
               changelog: this.state.detailsEdit.changelog
          })
          .then((response) => {

               //Show success alert
               this.setState({
                    editVersionSuccessInfo: true,
                    waitingForAction: false
               })

               this.props.downloadAppData(this.state.detailsEdit.appId);


               setTimeout( () => {
                    this.setState({editVersionSuccessInfo: false})

               }, 2000);

          })
          .catch((error) => {
               this.setState({                    
                    error: {
                         display: true,
                         title: "Edycja informacji o wersji aplikacji",
                         message: `Nie udało się edytować informacji o wersji aplikacji`,
                         data: error.response.data
               }});
          });
     }
     


     //After submit dependency form
     handleSubmitDependency = (e) => {
          e.preventDefault();

          let form = e.target;

          //Disable "ADD" button, prevent double add 
          form.querySelector('button').setAttribute("disabled", true)


          axios.post('/edocs-family-manager/db/dependencies', {
               parentAppVersionId: parseInt(e.target.appVersion.value, 10),
               childAppVersionId: this.state.detailsEdit.id
          })
          .then((responseDependencyAdd) => {
               if(responseDependencyAdd.status === 200) {
                    
                    this.setState({
                         addDependencySuccessInfo: true,
                         disabledAppVerInput: true,
                         selectedAppVerId: null
                    })

                    form.reset();
                    form.querySelector('button').removeAttribute("disabled");


                    this.getDependencies(this.state.detailsEdit.id);

                    setTimeout( () => {
                         this.setState({addDependencySuccessInfo: false})
                    }, 2000);
                    
               }
          })
          .catch((error) => {
               this.setState({                    
                    error: {
                         display: true,
                         title: "Dodawanie zależności",
                         message: "Nie udało się dodać zależności do bazy danych.",
                         data: error.response.data
               }});
          });


     }



     //After click "Delete" button
     dependencyDelete = (id) => {

          if (window.confirm("Jesteś pewien?")) {
               this.setState( {
                                   waitingForAction: true
                              } )
               
               axios.delete(`/edocs-family-manager/db/dependencies/${id}`)
               .then((response) => {
                              //Enabling possibility to click "delete version button" & download current data
                              this.setState( {
                                   waitingForAction: false
                              } )

                              this.getDependencies(this.state.detailsEdit.id);
               })
               .catch((error) => {
                    this.setState({                    
                         error: {
                              display: true,
                              title: "Usunięcie zależnosci",
                              message: `Nie udało się usunąć zależnosci.`,
                              data: error.response.data
                    }});
               });
          }
     }



     //Table of dependencies
     dependencyTable() {
          let table;
          let tableHead =          <thead>
                                        <tr>
                                             <th>ID (wersji aplikacji)</th>
                                             <th>Nazwa aplikacji</th>
                                             <th>Wersja</th>
                                             <th>Data utworzenia</th>
                                             <th>Opis zmian</th>
                                             <th>Usuń</th>
                                        </tr>
                                   </thead>


          if (this.state.dependencies !== null ) {

               //If dependencies is empty object
               if(Object.keys(this.state.dependencies).length === 0) {
                         table = 
                         <Table size="sm">
                              {tableHead}
                              <tbody>
                                   <tr>
                                        <td colSpan="6">
                                             Brak danych
                                        </td>
                                   </tr>
                              </tbody>
                         </Table>

                    return table;
               }
                    
               let tableBody = [];

               //Dla każdego klucza mapy...
               for (let key in this.state.dependencies) {  
                    
                    tableBody.push(<tr key={key}>
                                        <td colSpan="6">Rzędu: {key}</td>
                                   </tr>)

                    //Dla każdej wartości klucza mapy...
                    for (let value of this.state.dependencies[key]) {
                         tableBody.push(<tr key={JSON.stringify(value)}>
                                             <th scope="row">{value.id}</th>
                                             <th>{value.name}</th>
                                             <th>{value.version}</th>
                                             <th>{value.creationDate}</th>
                                             <th>{value.changelog}</th>
                                             <th><Button disabled={this.state.waitingForAction} onClick={ () => { this.dependencyDelete(value.dependencyId) } } color="danger">Usuń</Button></th>
                                        </tr>
                         )

                    }
               }

               table = 
                         <Table size="sm">
                              {tableHead}
                              <tbody>
                                   {tableBody}
                              </tbody>
                         </Table>

                    return table;
          } else {
               //If dependencies is null - waiting for data...
                         table = 
                         <Table size="sm">
                              {tableHead}
                              <tbody>
                                   <tr>
                                        <td colSpan="6">
                                             <Loading />
                                        </td>
                                   </tr>
                              </tbody>
                         </Table>

                    return table;
          }
     }





     render() {

     


          let allAppsArray = <option>{this.state.appsList}</option>;
          if(Array.isArray(this.state.appsList) ) {
               allAppsArray = this.state.appsList.map((element) =>
                    <option key={element.id} value={element.id}>{element.name}</option>
               );
               allAppsArray.unshift(<option key='default' value="" disabled={!this.state.disabledAppVerInput}>-- Wybierz aplikacje --</option>)
          }


          let allAppsVersionsArray = <option>{this.state.app_versions}</option>;
          if(Array.isArray(this.state.app_versions) ) {
               allAppsVersionsArray = this.state.app_versions.map((element) =>
                    <option key={element.id} value={element.id}>{element.version}</option>
               );
               allAppsVersionsArray.unshift(<option key='default' value="" disabled={this.state.selectedAppVerId !== null}>-- Wybierz wersje aplikacji --</option>)
          }
          

          let parentOfInfo = null;
          if(this.state.parentOfList.length > 0) {

               let parentOfInfoList = this.state.parentOfList.join(', ');

               parentOfInfo =  <Row >
                                   <Col> 
                                   Obecna wersja jest zależnością dla wersji aplikacji o ID:

                                   <Alert color="primary">
                                   {parentOfInfoList}
                                   </Alert>

                                   </Col>
                              </Row>

          }

     return (
          <section id="AppManageDetail">
               <h5>Edycja szczegółów wersji aplikacji<br /> <strong> {this.state.detailsEdit.id} </strong> ( {this.state.detailsEdit.version} )</h5>

               <h6>Tabela zależności od aplikacji:</h6>
                    <Row>
                         <Col className="dependency-table-container"> 
                         {this.dependencyTable()}
                         </Col>
                    </Row>


                    {parentOfInfo}


                    <Row>
                         <Col>

                         <h6>Dane wersji:</h6>

                         <Form className="client-data-form" onSubmit={this.handleSubmitData}>
                              <FormGroup>
                                   <Label for="creationDate">Data utworzenia:</Label>
                                   <Input type="date" name="creationDate" id="creationDate" value={this.state.detailsEdit.creationDate} onChange={this.handleChange} max="9999-12-31" required />
                              </FormGroup>

                              <FormGroup>
                                   <Label for="version">Numer wersji:</Label>
                                   <Input type="text" name="version" id="version" value={this.state.detailsEdit.version} onChange={this.handleChange} maxLength="16" required />
                              </FormGroup>

                              <FormGroup>
                                   <Label for="changelog">Opis zmian:</Label>
                                   <Input type="textarea" name="changelog" id="changelog" value={this.state.detailsEdit.changelog} onChange={this.handleChange}  required />
                              </FormGroup>


                              <Row>
                                   <Col sm="8">
                                        <Button disabled={this.state.waitingForAction} color="warning">Zapisz zmiany</Button>
                                   </Col>
                                   
                                   <Col sm="4">
                                        <Button disabled={this.state.waitingForAction} color="danger" onClick={ () => { this.props.handleVersionDeleteCallback(this.props.detailsEdit.id) } }>Usuń</Button>
                                   </Col>
                              </Row>


                              <Row>
                                   <Col>
                                        <Alert isOpen={this.state.editVersionSuccessInfo} color="success">
                                             Zmiany zostały zapisane
                                        </Alert>
                                   </Col>
                              </Row>


                         </Form>
                         </Col>



                         <Col>

                         <h6>Dodawanie zależności:</h6>

                         <Form onSubmit={this.handleSubmitDependency}>
 

                              <FormGroup>
                                   <Label for="appId">*Aplikacja</Label>
                                   <Input type="select" name="appId" id="appId" onChange={this.handle_appChange} required>
                                        {allAppsArray}
                                   </Input>
                              </FormGroup>

                              <FormGroup>
                                   <Label for="appVersion">*Wersja aplikacji</Label>
                                   <Input type="select" name="appVersion" id="appVersion" onChange={this.handle_appVerChange} disabled={this.state.disabledAppVerInput} required>
                                        {allAppsVersionsArray}
                                   </Input>
                              </FormGroup>



                              <Row>
                                   <Col>
                                   <Label for="appVersion">Potwierdzenie</Label>
                                        <Button type="submit" color="success">Dodaj</Button>   
                                        <Alert isOpen={this.state.addDependencySuccessInfo} color="success">
                                             Zależność dodana.
                                        </Alert>
                                   </Col>    
                              </Row> 
                         </Form>
                         </Col>

                    </Row>    

               <Error title={this.state.error.title} message={this.state.error.message} data={this.state.error.data} display={this.state.error.display} />

          </section>

          );
     }
}

