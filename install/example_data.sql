INSERT INTO "family_manager"."apps" ("id", "app_description", "app_rank", "create_date", "name") VALUES
(1,	'Główna część systemu Alphabet',	2,	'2017-02-04',	'EDOCS Alphabet - Main System'),
(2,	'Główna część systemu FaceBook',	2,	'2017-01-05',	'EDOCS FaceBook - Main System'),
(3,	'Aplikacja odpowiedzialna za wyświetlanie statystyk',	3,	'2017-06-14',	'EDOCS Statistics'),
(6,	'Komunikator WWW',	3,	'2017-08-12',	'EDOCS Communicator'),
(7,	'Tester samolotów',	2,	'2017-03-01',	'EDOCS Fly Tester'),
(5,	'Aplikacja umożliwiająca podgląd streamowanych video',	3,	'2017-07-13',	'EDOCS Video Controller'),
(4,	'Aplikacja odpowiedzialna za obsługe maili',	3,	'2017-06-03',	'EDOCS Mail System'),
(8,	'Serwery firmy',	2,	'2017-06-03',	'EDOCS Servers');




INSERT INTO "family_manager"."apps_versions" ("id", "app_id", "version", "creation_date", "changelog") VALUES
(1,	1,	'0.0.1.0',	'2018-01-01',	'Główne CORE systemu Alphabet'),
(2,	1,	'0.1.2.3',	'2018-01-05',	'Dodanie obsługi statystyk'),
(4,	2,	'0.1.0.0',	'2018-01-01',	'Główne CORE systemu FB'),
(5,	2,	'1.1.0.0',	'2018-01-07',	'Dodanie obsługi komunikatora'),
(6,	2,	'1.2.1.0',	'2018-01-14',	'Dodanie obsługi statystyk'),
(7,	3,	'0.1.0.0',	'2018-01-01',	'Baza dla aplikacji Statistics'),
(8,	3,	'0.1.0.1',	'2018-01-16',	'Statystyki spersonalizowane dla maili'),
(9,	3,	'0.1.1.0',	'2018-01-16',	'Poprawienie wyglądu statystyk'),
(10,	4,	'3.0.0.0',	'2018-01-21',	'Obsługa i wyświetlanie maili'),
(11,	4,	'4.0.0.0',	'2018-01-25',	'Dodana możliwość wysyłania maili'),
(13,	5,	'0.0.1.0',	'2018-01-08',	'Wyświetlanie wideo na stronie WWW'),
(14,	5,	'0.1.0.0',	'2018-01-09',	'Możliwość streamowania własnych video z kamerki'),
(15,	5,	'0.1.0.1',	'2018-01-11',	'Poprawienie błędów poprzedniej wersji'),
(16,	6,	'0.1.0.0',	'2018-01-11',	'Serwer komunikatora WWW'),
(17,	6,	'0.1.1.0',	'2018-01-12',	'Możliwość wyświetlania wiadomości z serwera na stronie'),
(18,	6,	'0.1.2.0',	'2018-01-13',	'Możliwość wysyłania wiadomości do serwera'),
(19,	7,	'2.0.0.0',	'2018-01-05',	'System kontroli samolotów'),
(3,	1,	'0.1.5.0',	'2018-01-22',	'Dodanie obsługi mailingu'),
(22,	8,	'0.0.0.1',	'2018-01-04',	'Postawienie maszyny'),
(23,	8,	'0.0.1.0',	'2018-01-05',	'Dodanie serwera mailowego'),
(24,	8,	'0.0.2.0',	'2018-01-06',	'Dodanie serwera WWW'),
(12,	4,	'5.0.0.0',	'2018-01-27',	'Dodana możliwość przekierowania maili z poziomu WWW');




INSERT INTO "family_manager"."clients_info" ("id", "companyname", "join_date", "patron", "description") VALUES
(1,	'Google',	'2018-02-01',	'Pracownik A',	'Klient o nazwie GOOGLE - o ID: 1'),
(2,	'FaceBook',	'2018-02-07',	'Pracownik A',	'Klient o nazwie FACEBOOK - o ID: 2'),
(5,	'General Electric',	'2018-03-03',	'Pracownik C',	'Klient o nazwie GENERAL ELECTRIC - o ID: 5'),
(6,	'Boeing',	'2018-02-05',	'Pracownik C',	'Klient o nazwie BOEING - o ID: 6'),
(7,	'Microsoft',	'2018-03-15',	'Pracownik B',	'Klient o nazwie MICROSOFT - o ID: 7'),
(4,	'AT&T',	'2018-02-20',	'Pracownik C',	'Klient o nazwie AT&T - o ID: 4'),
(3,	'Apple',	'2018-02-16',	'Pracownik B',	'Klient o nazwie APPLE - o ID: 3');





INSERT INTO "family_manager"."dependencies" ("id", "parent_app_version_id", "child_app_version_id") VALUES
(1,	1,	2),
(2,	7,	2),
(3,	1,	3),
(4,	11,	3),
(6,	17,	5),
(8,	7,	6),
(9,	11,	8),
(10,	23,	10),
(11,	23,	11),
(12,	24,	12),
(13,	24,	13),
(14,	24,	14),
(15,	24,	15),
(16,	24,	16),
(17,	24,	17),
(18,	24,	18),
(19,	16,	17),
(20,	16,	18),
(21,	22,	23),
(22,	22,	24);


INSERT INTO "family_manager"."clients_apps" ("id", "client_id", "implement_date", "disable_date", "app_version_id", "implement_description") VALUES
(1,	1,	'2018-02-03',	NULL,	3,	'Przypisanie klientowi GOOGLE najnowszej wersji apk alphabet'),
(2,	1,	'2018-02-02',	NULL,	9,	'Implementacja statystyk dla systemu Alphabet klienta GOOGLE'),
(3,	2,	'2018-02-06',	NULL,	6,	'Implementacja najnowszej wersji systemu FaceBook dla klienta'),
(4,	2,	'2018-02-02',	NULL,	8,	'Implementacja najnowszej wersji statystyk dla systemu FaceBook'),
(5,	2,	'2018-02-03',	NULL,	18,	'Dodanie najnowszej wersji aplikacji Communicator do systemu FaceBook dla klienta'),
(6,	1,	'2018-02-02',	NULL,	15,	'Implementacja najnowszej wersji obsługi i streamowania video dla klienta Google'),
(7,	2,	'2018-02-04',	NULL,	13,	'Implementacja aplikacji umożliwiającej jedynie wyświetlanie video na stronie dla systemu FaceBook'),
(10,	7,	'2018-02-02',	NULL,	18,	'Implementacja najnowszej wersji EDOCS Communicator pod potrzeby klienta'),
(11,	4,	'2018-02-03',	NULL,	9,	'Implementacja najnowszej wersji aplikacji Statistics - z poprawionym wyglądem'),
(12,	3,	'2018-02-02',	NULL,	15,	'Implementacja aplikacji związanej z obsługą video - download/stream'),
(13,	3,	'2018-02-04',	NULL,	12,	'Implementacja najnowszej wersji systemu obsługi maili'),
(9,	6,	'2018-02-03',	'2018-03-15',	19,	'Dodanie najnowszej wersji systemu kontroli samolotów
----------------
Klient poprosił o tymczasowe wyłączenie wsparcia
'),
(8,	5,	'2018-02-02',	'2018-03-19',	7,	'Implementacja jedynie bazy systemu statistic dla klienta General Electric
-------Klient zrezygnował z wersji 0.1.0.0'),
(14,	5,	'2018-03-19',	NULL,	8,	'Implementacja najnowszej wersji aplikacji');